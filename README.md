# [Binary Application Record Encoding (BARE)](https://baremessages.org/) for .NET
BareNET [![NuGet](https://badgen.net/nuget/v/BareNET)](https://www.nuget.org/packages/BareNET) | BareFs [![NuGet](https://badgen.net/nuget/v/BareFs)](https://www.nuget.org/packages/BareFs) | CLI [![NuGet](https://badgen.net/nuget/v/bare-cli)](https://www.nuget.org/packages/bare-cli)

BARE is a messaging protocol optimized for small messages and simplicity of implementation. It's lightweight protocol let's you build well defined schemata and with optional code generation you'll gain compatibility and type safety over a broad range of programming environments. BARE also puts an emphasis on backwards compatibility of messages, messages encoded today will be decodable tomorrow without sacrificing extensibility.
Thus far there exist implementations for Lisp, D, Elm, Erlang, Go, Java, Javascript, OCaml, PHP, Python, Ruby, Rust and many more.

This is the .NET implementation for BARE messaging with support for C# and F# projects, including an optional code generator.


[[_TOC_]]

## Why choose BARE messaging
Amongst the myriad of messaging protocols there are a few with wide adoption that became de-facto standards to choose from. The choice between Google protobuf, JSON, BSON, MessagePack and so forth is a choice between tradeoffs. JSON is human and machine readable, yet the messages are pretty bulky. Protobuf optimizes against that, yet is too fragile and more complicated. BSON has MongoDB implementation details leaked into the specifications... The list goes on and on. If you like to read a more detailed report of all the trade-offs considered, please indulge in the original blog post by the BARE creator Drew DeVault: [Introducing the BARE messaging encoding](https://drewdevault.com/2020/06/21/BARE-message-encoding.html)

BARE is for you, if you want to optimize for:
- Small messages
- Standardization
- Ease of implementation
- Universal implementability with little to no need for extensions
- Simplicity — no extra junk that isn’t contributing to the core mission


# BARE.NET Features
The .NET implemenation of BARE messaging fully supports the BARE's IETF internet-draft: [draft-devault-bare-08](https://datatracker.ietf.org/doc/draft-devault-bare/)

- Optimized for small messages: binary, not self-describing, no alignment or padding
- Standardized & simple: the specification is just over 1,000 words
- Universal: extensibility does not require expanding the implementation nor making messages which are incompatible with older implementations
- Zero dependencies
- Parsing of [BARE-schemata](https://datatracker.ietf.org/doc/html/draft-devault-bare-01#section-3)
Optional and specifically tailored to .NET
- [Code generation](Bare.CodeGen/CSharp_code_generator.cs) of types and encoding/decoding methods of [BARE-schemata](https://datatracker.ietf.org/doc/html/draft-devault-bare-01#section-3).
    - supports deeply nested anonymous structs and unions.
    - supports usage of types outside of code generation. More infos [here [C#]](./BareNET/README.md#inject-custom-types-into-generated-code) or [here [F#]](./BareFs/README.md#inject-custom-types-into-generated-code)

For a deeper understanding of the BARE basics, please refer to the [official documentation](https://baremessages.org/).

## Installation
The packages for BARE.NET can be installed via dotnet-CLI or [Paket](https://fsprojects.github.io/Paket/), depending on your preferences.

| Language | dotnet-CLI                 | Paket             |
| ---      | ---------------------------| ----------------- |
| C#       | <i>dotnet add package BareNET</i> | <i>paket add BareNET</i> |
| F#       | <i>dotnet add package BareFs</i>  | <i>paket add BareFs</i> |


To install the code generator you need .NET 5 or later installed on your system, then:
```cmd
dotnet tool install bare-cli
```

## Code generation
With your BARE message specification in a given file, let's assume the name `schema.bare`, to create your language specific types you can use the code generator, specify the target-language and the output file and it will create the respective types for you.
```bash
// For C#:
dotnet bare schema.bare MessageTypes.cs --lang cs

// For F#:
dotnet bare schema.bare MessageTypes.fs --lang fs
```
> :warning: Remember to include the generated file in the C#/F# project


The content of the source file schema.bare:
```
type Person struct { Name: str  Age: i32 }
```

<details>
<summary>
The generated `MessageTypes.cs`
</summary>

```csharp
//////////////////////////////////////////////////
// Generated code by BareNET - 1/12/2023 3:07AM //
//////////////////////////////////////////////////
using System;
using System.Linq;
using System.Collections.Generic;
namespace Bare.Msg
{
	public readonly struct Person
	{
		public readonly string Name;
		public readonly int Age;
	
		public Person(string name, int age)
		{
			Name = name;
			Age = age;
		}
	
		public byte[] Encoded()
		{
			return BareNET.Bare.Encode_string(Name)
				.Concat(BareNET.Bare.Encode_i32(Age))
				.ToArray();
		}
	
		public static Person Decoded(byte[] data) { return Decode(data).Item1; }
	
		public static ValueTuple<Person, byte[]> Decode(byte[] data)
		{
			var name = BareNET.Bare.Decode_string(data);
			var age = BareNET.Bare.Decode_i32(name.Item2);
			return new ValueTuple<Person, byte[]>(
				new Person(name.Item1, age.Item1),
				age.Item2);
		}
	}
}
```
</details>

<details>
<summary>
The generated `MessageTypes.fs`
</summary>

```fsharp
//////////////////////////////////////////////////
// Generated code by BareNET - 1/12/2023 3:06AM //
//////////////////////////////////////////////////
namespace Bare.Msg

type Person =
  {
    Name: string
    Age: int
  }

module Encoding =
  let ofPerson (value: Person) : BareNET.Encoding_Result =
    (BareNET.Bare.success (BareNET.Bare.encode_string)) value.Name
    |> BareNET.Bare.andThen (BareNET.Bare.success (BareNET.Bare.encode_i32)) value.Age

  let decode_Person : BareNET.Decoder<Person> =
    BareNET.Bare.decode_complex (fun (name: string) (age: int) -> { Person.Name = name ; Age = age })
    |> BareNET.Bare.apply (BareNET.Bare.decode_string)
    |> BareNET.Bare.apply (BareNET.Bare.decode_i32)

  let toPerson (data: byte array) : Result<Person, string> =
    decode_Person data |> Result.map fst
```
</details>

> :bulb: The common way is to store the schema into a `schema.bare` file and provide the file path to the bare-cli. For more options see `dotnet bare --help`!

### About .NET specific types
Some types like `Guid`, `DateTime` or `decimal` are not supported in BARE. In order to use custom types the BARE schema needs custom encoders and decoders to map from and to the custom types.
bare-cli provides a way to inject custom types into generated code. See [BARE.NET Documentation for C#](./BareNET/README.md#inject-custom-types-into-generated-code) or [BARE.NET Documentation for F#](./BareFs/README.md#inject-custom-types-into-generated-code).

## Example
Below you'll find a glimpse of how to use BARE.NET in production. For more detailed information please read the [BARE.NET Documentation for C# projects](./BareNET/README.md) or the [BARE.NET Documentation for F# projects](./BareFs/README.md).

#### Using BARE.NET within C# projects
```csharp
using Bare.Msg; // Default namespace for generated code

var person = new Person(name: "Nils Helmig", age: 25);

try
{
    var encoded = person.Encoded(); // Structs have an Encoded Method. Unions and enums encoders are available in the generated static `Encoding` class

    // send bytes over a socket
    socket.SendBytes(encoded);
    // or store them in a file
    file.WriteAll(encoded, "person.bin");
    // or maybe encrypt the data
    encryptBytes(encoded);
}
catch (ArgumentException ex)
{
    Console.WriteLine($"The encoding of Person failed with {ex.Message}");
}


byte[] data = // bytes received from a socket, read from or file, or another way.

try
{
    var person = Person.Decoded(data);
    Console.WriteLine($"Here we have the {person.Age} old {person.Name}");
}
catch (ArgumentException ex)
{
    Console.WriteLine($"The decoding of Person failed with {ex.Message}");
}
```

#### Using BARE.NET within F# projects
```fsharp
open Bare.Msg // Default namespace for generated code

let person : Person = { Name = "Nils Helmig" ; Age = 25 }

match person |> Encoding.ofPerson with // Encoding is the generated module for encoders and decoders
| Ok encoded -> // encoded : byte array
    // send bytes over a socket
    socket.SendBytes encoded
    // or store them in a file
    encoded |> File.WriteTo "person.bin"
    // or maybe encrypt the data
    encryptBytes encoded

| Error error ->
    printf "The encoding of Person failed with %s" error

let data : byte array = // bytes received from a socket, read from or file, or another way.
match data |> Encoding.toPerson with
| Ok person ->
    printf "%s is %i years old" person.Name person.Age

| Error error ->
    printf "The decoding of Person failed with %s" error
```
