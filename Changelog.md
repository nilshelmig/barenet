# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2023-01-12
### Changed
- [AST] Grammar from BARE v7 is now implemented.

## [0.2.0] - 2021-03-03
### Fixed
- [AST] Parser is now specification conform. See #2

## [0.1.2] - 2021-03-03
### Added
- Added helper method `Encode_union_case` encode single union case.
- Added overload for `Decode_union` to use a function instead of a dictionary to determine the decoder.

### Fixed
- Encoding of nested unions.

## [0.1.1] - 2021-02-14
### Fixed
- [AST] Parser can now handle end of stream and won't fail if a schema ends with a trailing newline. See #1

## [0.1.0] - 2021-02-10
### Added
- Initial release