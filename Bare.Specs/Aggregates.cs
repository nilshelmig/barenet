﻿using System;
using System.Collections.Generic;
using System.Linq;
using BareNET;
using NUnit.Framework;

namespace Bare.Specs
{
    [TestFixture]
    public class Aggregates
    {
        internal interface TestUnion { }

        internal readonly struct FirstCase : TestUnion { }
        internal interface NestedUnion : TestUnion { }
        internal readonly struct NestedCase : NestedUnion { }

        [Test]
        public void Type_optional_encodes_no_value_as_boolean_false()
        {
            Assert.AreEqual(new byte[] { 0x00 }, BareNET.Bare.Encode_optional<sbyte>(null, BareNET.Bare.Encode_i8));
        }

        [Test]
        public void Type_optional_encodes_value_as_boolean_true_followed_by_encoded_value()
        {
            Assert.AreEqual(new byte[] { 0x01, 0x1B }, BareNET.Bare.Encode_optional<sbyte>(27, BareNET.Bare.Encode_i8));
        }

        [Test]
        public void Type_optional_only_takes_needed_bytes()
        {
            var (_, rest_no_value) = BareNET.Bare.Decode_optional(new byte[] { 0x00, 0x13, 0xA6, 0xE0 }, BareNET.Bare.Decode_i8);
            Assert.AreEqual(new byte[] { 0x13, 0xA6, 0xE0 }, rest_no_value);

            var (_, rest_value) = BareNET.Bare.Decode_optional(new byte[] { 0x01, 0x27, 0x13, 0xA6, 0xE0 }, BareNET.Bare.Decode_i8);
            Assert.AreEqual(new byte[] { 0x13, 0xA6, 0xE0 }, rest_value);
        }

        [Test]
        public void Type_optional()
        {
            Assert.AreEqual(20, BareNET.Bare.Decode_optional(BareNET.Bare.Encode_optional<int>(20, BareNET.Bare.Encode_i32), BareNET.Bare.Decode_i32).Item1);
            Assert.AreEqual(null, BareNET.Bare.Decode_optional(BareNET.Bare.Encode_optional<int>(null, BareNET.Bare.Encode_i32), BareNET.Bare.Decode_i32).Item1);
            Assert.AreEqual(9313u, BareNET.Bare.Decode_optional(BareNET.Bare.Encode_optional<uint>(9313u, BareNET.Bare.Encode_u32), BareNET.Bare.Decode_u32).Item1);
            Assert.AreEqual(null, BareNET.Bare.Decode_optional(BareNET.Bare.Encode_optional<uint>(null, BareNET.Bare.Encode_u32), BareNET.Bare.Decode_u32).Item1);
            Assert.AreEqual(true, BareNET.Bare.Decode_optional(BareNET.Bare.Encode_optional<bool>(true, BareNET.Bare.Encode_bool), BareNET.Bare.Decode_bool).Item1);
            Assert.AreEqual(null, BareNET.Bare.Decode_optional(BareNET.Bare.Encode_optional<bool>(null, BareNET.Bare.Encode_bool), BareNET.Bare.Decode_bool).Item1);
            Assert.AreEqual("Hello World", BareNET.Bare.Decode_optional_ref(BareNET.Bare.Encode_optional_ref("Hello World", BareNET.Bare.Encode_string), BareNET.Bare.Decode_string).Item1);
            Assert.AreEqual(null, BareNET.Bare.Decode_optional_ref(BareNET.Bare.Encode_optional_ref<string>(null, BareNET.Bare.Encode_string), BareNET.Bare.Decode_string).Item1);
        }

        [Test]
        public void Type_list_fixed_length_encodes_data_literally_in_the_message()
        {
            var data = BareNET.Bare.Encode_list_fixed_length(4, new byte[] { 0x01, 0x02, 0x03, 0x04 }, BareNET.Bare.Encode_u8);
            Assert.AreEqual(new byte[] { 0x01, 0x02, 0x03, 0x04 }, data);
        }

        [Test]
        public void Type_list_fixed_length_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_list_fixed_length(4, new byte[] { 0x01, 0x02, 0x03, 0x04, 0xF2, 0xAC }, BareNET.Bare.Decode_u8);
            Assert.AreEqual(new byte[] { 0xF2, 0xAC }, rest);
        }

        [Test]
        public void Type_list_fixed_length__given_data_is_less_than_given_length_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Encode_list_fixed_length(5, new byte[] { 0x02, 0x03 }, BareNET.Bare.Encode_u8); });
        }

        [Test]
        public void Type_list_fixed_length__zero_length_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Encode_list_fixed_length(0, new byte[0], BareNET.Bare.Encode_u8); });
        }

        private static T[] Rounttrip_list_fixed_length<T>(int length, IEnumerable<T> value, BareEncoder<T> encoder, BareDecoder<T> decoder)
        {
            return BareNET.Bare
                .Decode_list_fixed_length(length, BareNET.Bare.Encode_list_fixed_length(length, value, encoder),
                    decoder).Item1.ToArray();
        }

        [Test]
        public void Type_list_fixed_length()
        {
            Assert.AreEqual(new [] { 4, 19, 10, 2246, 93 }, Rounttrip_list_fixed_length(5, new[] { 4, 19, 10, 2246, 93 }, BareNET.Bare.Encode_i32, BareNET.Bare.Decode_i32));
            Assert.AreEqual(new[] { "Hello", "World" }, Rounttrip_list_fixed_length(2, new[] { "Hello", "World" }, BareNET.Bare.Encode_string, BareNET.Bare.Decode_string));
            Assert.AreEqual(new float?[] { null, 13.31f, 9.3157f, null, null, 3.113f }, Rounttrip_list_fixed_length(6, new float?[] { null, 13.31f, 9.3157f, null, null, 3.113f }, value => BareNET.Bare.Encode_optional(value, BareNET.Bare.Encode_f32), data => BareNET.Bare.Decode_optional(data, BareNET.Bare.Decode_f32)));
        }

        [Test]
        public void Type_list_encodes_length_of_values_as_uint_in_first_place_followed_by_the_encoded_values()
        {
            Assert.AreEqual(new byte[] { 0x02, 0x04, 0x00, 0x31, 0x01 }, BareNET.Bare.Encode_list(new short[] { 4, 305 }, BareNET.Bare.Encode_i16));
        }

        [Test]
        public void Type_list_only_takes_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_list(new byte[] { 0x04, 0x01, 0x02, 0x03, 0x04, 0xF2, 0xAC }, BareNET.Bare.Decode_u8);
            Assert.AreEqual(new byte[] { 0xF2, 0xAC }, rest);
        }

        [Test]
        public void Type_list__zero_length()
        {
            Assert.AreEqual(new byte[] { 0x00 }, BareNET.Bare.Encode_list(new List<byte>(), BareNET.Bare.Encode_u8));
        }

        private static T[] Rounttrip_list<T>(IEnumerable<T> values, BareEncoder<T> encoder, BareDecoder<T> decoder)
        {
            return BareNET.Bare.Decode_list(BareNET.Bare.Encode_list(values, encoder), decoder).Item1.ToArray();
        }

        [Test]
        public void Type_list()
        {
            Assert.AreEqual(new[] { 4, 19, 10, 2246, 93 }, Rounttrip_list(new[] { 4, 19, 10, 2246, 93 }, BareNET.Bare.Encode_i32, BareNET.Bare.Decode_i32));
            Assert.AreEqual(new[] { "Hello", "World" }, Rounttrip_list(new[] { "Hello", "World" }, BareNET.Bare.Encode_string, BareNET.Bare.Decode_string));
            Assert.AreEqual(new float?[] { null, 13.31f, 9.3157f, null, null, 3.113f }, Rounttrip_list(new float?[] { null, 13.31f, 9.3157f, null, null, 3.113f }, value => BareNET.Bare.Encode_optional(value, BareNET.Bare.Encode_f32), data => BareNET.Bare.Decode_optional(data, BareNET.Bare.Decode_f32)));
        }

        [Test]
        public void Type_map_encodes_length_of_values_as_uint_in_first_place_followed_by_encoded_key_value_pairs_concatenated()
        {
            Assert.AreEqual(
                new byte[] { 0x01, 0x0D, 0x00, 0x0B, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64 }, 
                BareNET.Bare.Encode_map(
                    new Dictionary<short, string> { { 13, "Hello World" } },
                    BareNET.Bare.Encode_i16,
                    BareNET.Bare.Encode_string));
        }

        [Test]
        public void Type_map_zero_length()
        {
            Assert.AreEqual(
                new byte[] { 0x00 },
                BareNET.Bare.Encode_map(
                    new Dictionary<short, string>(),
                    BareNET.Bare.Encode_i16,
                    BareNET.Bare.Encode_string));
        }

        [Test]
        public void Type_map_only_takes_needed_bytes()
        {
            var (_, rest) =
                BareNET.Bare.Decode_map(
                    new byte[] { 0x02, 0x01, 0x00, 0x02, 0x00, 0xFE, 0x2A },
                    BareNET.Bare.Decode_u8,
                    BareNET.Bare.Decode_i8);
            Assert.AreEqual(new byte[] { 0xFE, 0x2A }, rest);
        }

        [Test]
        public void Type_map_duplicated_key_throws_exception()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                BareNET.Bare.Decode_map(
                    new byte[] {0x02, 0x01, 0x00, 0x01, 0x00},
                    BareNET.Bare.Decode_u8,
                    BareNET.Bare.Decode_i8);
            });
        }

        [Test]
        public void Type_map()
        {
            var (decoded, _) = BareNET.Bare.Decode_map(
                BareNET.Bare.Encode_map(
                    new Dictionary<short, string>
                    {
                        { 13, "Hello World" },
                        { 14, "How are you?" },
                        { 25, "This is cool!" }
                    },
                    BareNET.Bare.Encode_i16,
                    BareNET.Bare.Encode_string),
                BareNET.Bare.Decode_i16,
                BareNET.Bare.Decode_string);

            Assert.AreEqual(
                new Dictionary<short, string>
                {
                    { 13, "Hello World" },
                    { 14, "How are you?" },
                    { 25, "This is cool!" }
                },
                decoded);
        }

        [Test]
        public void Type_union_encodes_type_identifier_followed_by_the_encoded_value()
        {
            var typeIdentifiers =
                new Dictionary<Type, (uint, BareEncoder<object>)>
                {
                    { typeof(int), (0, value => BareNET.Bare.Encode_i32((int)value)) },
                    { typeof(string), (1, value => BareNET.Bare.Encode_string((string)value)) }
                };

            Assert.AreEqual(
                new byte[] { 0x01, 0x0B, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64 },
                BareNET.Bare.Encode_union("Hello World", typeIdentifiers));
            Assert.AreEqual(
                new byte[] { 0x00, 0x05, 0x00, 0x00, 0x00 },
                BareNET.Bare.Encode_union(5, typeIdentifiers));
        }

        [Test]
        public void Type_union_type_without_encoder_throws_exception()
        {
            var encoders =
                new Dictionary<Type, (uint, BareEncoder<object>)>
                {
                    { typeof(int), (0, value => BareNET.Bare.Encode_i32((int)value)) },
                    { typeof(string), (1, value => BareNET.Bare.Encode_string((string)value)) }
                };

            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Encode_union(2.3f, encoders); });
        }

        [Test]
        public void Type_union_decodes_value_by_using_registered_decoder_by_identifier()
        {
            var decoders =
                new Dictionary<uint, BareDecoder<object>>
                {
                    {0, data => BareNET.Bare.Decode_i32(data)},
                    {1, data => BareNET.Bare.Decode_string(data)}
                };

            Assert.AreEqual(2, BareNET.Bare.Decode_union(new byte[] { 0x00, 0x02, 0x00, 0x00, 0x00 }, decoders).Item1);
            Assert.AreEqual("Hello World", BareNET.Bare.Decode_union(new byte[] { 0x01, 0x0B, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64 }, decoders).Item1);
        }

        [Test]
        public void Type_union_decoders_without_identifier_throws_exception()
        {
            var decoders = new Dictionary<uint, BareDecoder<object>> { {0, data => BareNET.Bare.Decode_i32(data)} };

            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Decode_union(new byte[] { 0x01, 0xFA, 0x02, 0x00, 0x00 }, decoders); });
        }

        [Test]
        public void Type_union_only_takes_needed_bytes()
        {
            var decoders = new Dictionary<uint, BareDecoder<object>> { {0, data => BareNET.Bare.Decode_i32(data)} };
            var (_, rest) = BareNET.Bare.Decode_union(new byte[] { 0x00, 0x02, 0x00, 0x00, 0x00, 0x3C, 0xBA, 0x1D }, decoders);

            Assert.AreEqual(new byte[] { 0x3C, 0xBA, 0x1D }, rest);
        }

        [Test]
        public void Type_union_encodes_and_decodes_nested_union()
        {
            var nestedUnion =
                Union<NestedUnion>.Register()
                    .With_Case<NestedCase>(v => new byte[0], d => (new NestedCase(), d));
            var union =
                Union<TestUnion>.Register()
                    .With_Case<FirstCase>(v => new byte[0], d => (new FirstCase(), d))
                    .With_Case<NestedUnion>(v => BareNET.Bare.Encode_union((NestedUnion)v, nestedUnion), d => BareNET.Bare.Decode_union<NestedUnion>(d, nestedUnion));

            var value = new NestedCase();
            var encoded = BareNET.Bare.Encode_union<TestUnion>(value, union);
            var (decoded, _) = BareNET.Bare.Decode_union<TestUnion>(encoded, union);
            Assert.AreEqual(value, decoded);
        }

        [Test]
        public void Type_union()
        {
            var union =
                Union<object>.Register()
                    .With_Case<int>(v => BareNET.Bare.Encode_i32((int)v), d => BareNET.Bare.Decode_i32(d))
                    .With_Case<string>(v => BareNET.Bare.Encode_string((string)v), d => BareNET.Bare.Decode_string(d))
                    .With_Case_identified_by<float>(2, v => BareNET.Bare.Encode_f32((float)v), d => BareNET.Bare.Decode_f32(d))
                    .With_Case<bool>(v => BareNET.Bare.Encode_bool((bool)v), d => BareNET.Bare.Decode_bool(d))
                    .With_Case<ulong>(value => BareNET.Bare.Encode_uint((ulong)value), data => BareNET.Bare.Decode_uint(data))
                    .With_Case<char>(v => BareNET.Bare.Encode_i32(Convert.ToInt32((char)v)), d => { var (v, r) = BareNET.Bare.Decode_i32(d); return ((char)v, r); })
                    .With_Case<byte[]>(v => BareNET.Bare.Encode_data((byte[])v), d => BareNET.Bare.Decode_data(d));
            var values = new object[]
            {
                2,
                2.32f,
                "Hello World",
                true,
                2UL,
                'A',
                new byte[] { 0x01, 0x02, 0x03, 0x04 }
            };

            foreach (var value in values)
            {
                var (decoded, _) = BareNET.Bare.Decode_union<object>(BareNET.Bare.Encode_union(value, union), union);
                Assert.AreEqual(value, decoded);
            }
        }
    }
}