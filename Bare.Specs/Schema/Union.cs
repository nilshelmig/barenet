using System;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class Union
    {
        [Test]
        public void Can_be_defined_in_one_line_with_single_member()
        {
            Expect.Schema("type NamedUnion union { i32 }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null)
                    })));
        }

        [Test]
        public void Can_be_defined_in_one_line_with_single_member_with_identifier()
        {
            Expect.Schema("type NamedUnion union { i32 = 1 }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), 1u)
                    })));
        }

        [Test]
        public void Can_be_defined_in_one_line_with_multiple_members_seperated_by_pipe()
        {
            Expect.Schema("type NamedUnion union { i32 | i64 | f32 }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null),
                        new UnionMember(new PrimitiveType(TypeKind.I64), null),
                        new UnionMember(new PrimitiveType(TypeKind.F32), null)
                    })));
        }
        
        [Test]
        public void Can_be_defined_in_multiple_lines_with_single_member()
        {
            Expect.Schema(@"
                type NamedUnion union
                    {
                        i32
                    }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null)
                    })));
        }
        
        [Test]
        public void Can_be_defined_in_multiple_lines_with_multiple_members_seperated_by_pipe()
        {
            Expect.Schema(@"
                type NamedUnion union
                    { i32
                    | i64
                    | f32
                    }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null),
                        new UnionMember(new PrimitiveType(TypeKind.I64), null),
                        new UnionMember(new PrimitiveType(TypeKind.F32), null)
                    })));
        }

        [Test]
        public void Can_start_a_single_member_with_a_pipe()
        {
            Expect.Schema("type NamedUnion union { | i32 = 1 }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), 1u)
                    })));
        }

        [Test]
        public void Can_end_a_single_member_with_a_pipe()
        {
            Expect.Schema("type NamedUnion union { i32 = 1 | }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), 1u)
                    })));
        }

        [Test]
        public void Can_start_and_end_a_single_member_with_a_pipe()
        {
            Expect.Schema("type NamedUnion union { | i32 = 1 | }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), 1u)
                    })));
        }
        
        [Test]
        public void Can_start_a_every_member_with_a_pipe()
        {
            Expect.Schema(@"
                type NamedUnion union
                    {
                        | i32
                        | i64
                        | f32
                    }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null),
                        new UnionMember(new PrimitiveType(TypeKind.I64), null),
                        new UnionMember(new PrimitiveType(TypeKind.F32), null)
                    })));
        }
        
        [Test]
        public void Can_start_a_every_member_with_a_pipe_and_end_with_a_pipe()
        {
            Expect.Schema(@"
                type NamedUnion union
                    {
                        | i32
                        | i64
                        | f32
                        |
                    }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null),
                        new UnionMember(new PrimitiveType(TypeKind.I64), null),
                        new UnionMember(new PrimitiveType(TypeKind.F32), null)
                    })));
        }
        
        [Test]
        public void Can_mix_members_with_and_without_identifier()
        {
            Expect.Schema(@"
                type NamedUnion union
                    { i32
                    | i64 = 5
                    | f32
                    }")
                .To_define_user_types(
                    new UserType("NamedUnion", new UnionType(new []
                    {
                        new UnionMember(new PrimitiveType(TypeKind.I32), null),
                        new UnionMember(new PrimitiveType(TypeKind.I64), 5u),
                        new UnionMember(new PrimitiveType(TypeKind.F32), null)
                    })));
        }

        [Test]
        public void Shall_have_brackets()
        {
            Expect.Schema("type NamedUnion union")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Shall_have_at_least_one_member()
        {
            Expect.Schema("type NamedUnion union {}")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Cannot_ommit_identifier_when_equals_if_given()
        {
            Expect.Schema("type NamedUnion union { i32 = }")
                .To_fail_parsing_with<FormatException>();
        }
    }
}