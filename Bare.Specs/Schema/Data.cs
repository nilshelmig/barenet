using System;
using System.IO;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class Data
    {
        [Test]
        public void Can_be_defined_with_length_in_brackets()
        {
            Expect.Schema("type SomeData data[4]")
                .To_define_user_types(
                    new UserType("SomeData", new DataType(4)));
        }

        [Test]
        public void Can_be_defined_without_length()
        {
            Expect.Schema("type SomeData data")
                .To_define_user_types(
                    new UserType("SomeData", new DataType(null)));
        }

        [Test]
        public void Shall_not_be_defined_without_length_size()
        {
            Expect.Schema("type SomeData data[]")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Length_should_be_at_least_one()
        {
            Expect.Schema("type SomeData data[0]")
                .To_fail_parsing_with<InvalidDataException>();
        }
    }
}