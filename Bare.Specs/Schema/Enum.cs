using System;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class Enum
    {
        [Test]
        public void Value_name_must_be_written_in_all_uppercase()
        {
            Expect.Schema("type NamedEnum enum { FIRSTVALUE }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] { new EnumValue("FIRSTVALUE", null) })));
        }

        [Test]
        public void Can_be_defined_in_single_line_with_single_value()
        {
            Expect.Schema("type NamedEnum enum { FIRST }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] { new EnumValue("FIRST", null) })));
        }

        [Test]
        public void Can_be_defined_in_single_line_with_multiple_values()
        {
            Expect.Schema("type NamedEnum enum { FIRST SECOND THIRD }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] {
                        new EnumValue("FIRST", null),
                        new EnumValue("SECOND", null),
                        new EnumValue("THIRD", null)
                    })));
        }

        [Test]
        public void Can_be_defined_in_multiple_lines_with_single_value()
        {
            Expect.Schema(@"
                type NamedEnum enum
                    {
                        FIRST
                    }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] { new EnumValue("FIRST", null) })));
        }

        [Test]
        public void Can_be_defined_in_multiple_lines_with_multiple_values()
        {
            Expect.Schema(@"
                type NamedEnum enum
                    {
                        FIRST
                        SECOND
                        THIRD
                    }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] {
                        new EnumValue("FIRST", null),
                        new EnumValue("SECOND", null),
                        new EnumValue("THIRD", null)
                    })));
        }

        [Test]
        public void Value_can_have_explicit_number_associated()
        {
            Expect.Schema("type NamedEnum enum { FIRST = 1 }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] { new EnumValue("FIRST", 1u) })));
        }

        [Test]
        public void Can_simultaneously_have_value_with_explicit_number_associated_and_value_without_number()
        {
            Expect.Schema(@"
                type NamedEnum enum
                {
                    FIRST = 1
                    SECOND
                    THIRD = 3
                }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new []
                        {
                            new EnumValue("FIRST", 1u),
                            new EnumValue("SECOND", null),
                            new EnumValue("THIRD", 3u),
                        })));
        }

        [Test]
        public void Must_have_at_least_one_value()
        {
            Expect.Schema("type NamedEnum enum {}")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Value_name_can_contain_underscore()
        {
            Expect.Schema("type NamedEnum enum { FIRST_VALUE }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] { new EnumValue("FIRST_VALUE", null) })));
        }

        [Test]
        public void Value_name_can_contain_digits()
        {
            Expect.Schema("type NamedEnum enum { FRIENDS4EVER }")
                .To_define_user_types(
                    new UserType("NamedEnum", new EnumType(new [] { new EnumValue("FRIENDS4EVER", null) })));
        }

        [Test]
        public void Value_name_shall_not_contain_lowercase_character()
        {
            Expect.Schema("type NamedEnum enum { FirstValue }")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Value_name_shall_not_start_with_a_digit()
        {
            Expect.Schema("type NamedEnum enum { 1ST }")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Parse_schema_fails_when_enum_value_begins_with_an_underscore()
        {
            Expect.Schema("type NamedEnum enum { _FIRST }")
                .To_fail_parsing_with<FormatException>();
        }
    }
}