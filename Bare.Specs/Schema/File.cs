using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class File
    {
        [Test]
        public void Can_be_empty()
        {
            Expect.Schema("").To_define_no_user_types();
        }

        [Test]
        public void Can_only_be_whitespaces()
        {
            Expect.Schema("                             ")
                .To_define_no_user_types();
        }

        [Test]
        public void Can_only_be_newlines()
        {
            Expect.Schema(@"
            
            
            
            
            
            
            
            
            ")
                .To_define_no_user_types();
        }

        [Test]
        public void Can_have_trailing_newline()
        {
            Expect.Schema(@"
                type PublicKey data[128]

                ")
                .To_define_user_types(new UserType("PublicKey", new DataType(128)));
        }

        [Test]
        public void Parse_schema()
        {
            Expect.Schema(@"
type PublicKey data[128]
type Time str # ISO 8601

type Department enum {
  ACCOUNTING
  ADMINISTRATION
  CUSTOMER_SERVICE
  DEVELOPMENT

  # Reserved for the CEO
  JSMITH = 99
}

type Customer struct {
  name: str
  email: str
  address: Address
  orders: list<struct {
    orderId: i64
    quantity: i32
  }>
  metadata: map<str><data>
}

type Employee struct {
  name: str
  email: str
  address: Address
  department: Department
  hireDate: Time
  publicKey: optional<PublicKey>
  metadata: map<str><data>
}

type TerminatedEmployee void

type Person union { Customer = 1 | Employee | TerminatedEmployee }

type Address struct {
  address: list<str>[4]
  city: str
  state: str
  country: str
}
")
            .To_define_user_types(
                new UserType("PublicKey", new DataType(128)),
                new UserType("Time", new PrimitiveType(TypeKind.String)),
                new UserType("Department", new EnumType(new []
                {
                    new EnumValue("ACCOUNTING", null),
                    new EnumValue("ADMINISTRATION", null),
                    new EnumValue("CUSTOMER_SERVICE", null),
                    new EnumValue("DEVELOPMENT", null),
                    new EnumValue("JSMITH", 99u),
                })),
                new UserType("Customer", new StructType(new []
                {
                    new StructField("name", new PrimitiveType(TypeKind.String)),
                    new StructField("email", new PrimitiveType(TypeKind.String)),
                    new StructField("address", new UserTypeName("Address")),
                    new StructField("orders", new ListType(new StructType(new []
                    {
                        new StructField("orderId", new PrimitiveType(TypeKind.I64)),
                        new StructField("quantity", new PrimitiveType(TypeKind.I32)),
                    }), null)),
                    new StructField("metadata", new MapType(
                        new PrimitiveType(TypeKind.String),
                        new DataType(null)
                    ))
                })),
                new UserType("Employee", new StructType(new []
                {
                    new StructField("name", new PrimitiveType(TypeKind.String)),
                    new StructField("email", new PrimitiveType(TypeKind.String)),
                    new StructField("address", new UserTypeName("Address")),
                    new StructField("department", new UserTypeName("Department")),
                    new StructField("hireDate", new UserTypeName("Time")),
                    new StructField("publicKey", new OptionalType(new UserTypeName("PublicKey"))),
                    new StructField("metadata", new MapType(
                        new PrimitiveType(TypeKind.String),
                        new DataType(null)
                    ))
                })),
                new UserType("TerminatedEmployee", new PrimitiveType(TypeKind.Void)),
                new UserType("Person", new UnionType(new []
                {
                    new UnionMember(new UserTypeName("Customer"), 1),
                    new UnionMember(new UserTypeName("Employee"), null),
                    new UnionMember(new UserTypeName("TerminatedEmployee"), null)
                })),
                new UserType("Address", new StructType(new []
                {
                    new StructField("address", new ListType(new PrimitiveType(TypeKind.String), 4)),
                    new StructField("city", new PrimitiveType(TypeKind.String)),
                    new StructField("state", new PrimitiveType(TypeKind.String)),
                    new StructField("country", new PrimitiveType(TypeKind.String))
                })));
        }
    }
}