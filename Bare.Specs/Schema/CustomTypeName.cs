﻿using System;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class CustomTypeName
    {
        [Test]
        public void Name_can_contain_digits()
        {
            Expect.Schema("type Number64 i64")
                .To_define_user_types(
                    new UserType("Number64", new PrimitiveType(TypeKind.I64)));
        }

        [Test]
        public void Name_shall_not_begin_with_a_lowercase_character()
        {
            Expect.Schema("type namedEnum u8")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Name_shall_not_begin_with_a_digit()
        {
            Expect.Schema("type 2NamedType i8")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Name_shall_not_contain_a_special_character()
        {
            Expect.Schema("type Überfähre i8")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Name_shall_not_contain_an_underscore()
        {
            Expect.Schema("type Back_to_you i8")
                .To_fail_parsing_with<FormatException>();
        }
    }
}