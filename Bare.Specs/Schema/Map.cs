using System;
using System.IO;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class Map
    {
        [Test]
        public void Defines_key_and_value_types()
        {
            Expect.Schema("type SomeMap map<str><i32>")
                .To_define_user_types(
                    new UserType(
                        "SomeMap",
                        new MapType(
                            new PrimitiveType(TypeKind.String),
                            new PrimitiveType(TypeKind.I32))));
        }

        [Test]
        public void Types_can_contain_whitespaces()
        {
            Expect.Schema("type SomeMap map < str > < i32 >")
                .To_define_user_types(
                    new UserType(
                        "SomeMap",
                        new MapType(
                            new PrimitiveType(TypeKind.String),
                            new PrimitiveType(TypeKind.I32))));
        }

        [Test]
        public void Type_shall_be_surrounded_by_angles()
        {
            Expect.Schema("type SomeMap map f32 i32")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Key_type_must_be_of_primitive_type()
        {
            Expect.Schema(@"
                type U8Map map<u8><data>
                type U16Map map<u16><data>
                type U32Map map<u32><data>
                type U64Map map<u64><data>
                type UIntMap map<uint><data>
                type I8Map map<i8><data>
                type I16Map map<i16><data>
                type I32Map map<i32><data>
                type I64Map map<i64><data>
                type IntMap map<int><data>
                type BoolMap map<bool><data>
                type StrMap map<str><data>
                type EnumMap map<enum { KEY }><data>
            ")
                .To_define_user_types(
                    new UserType(
                        "U8Map",
                        new MapType(
                            new PrimitiveType(TypeKind.U8), new DataType(null))
                    ),
                    new UserType(
                        "U16Map",
                        new MapType(
                            new PrimitiveType(TypeKind.U16), new DataType(null))
                    ),
                    new UserType(
                        "U32Map",
                        new MapType(
                            new PrimitiveType(TypeKind.U32), new DataType(null))
                    ),
                    new UserType(
                        "U64Map",
                        new MapType(
                            new PrimitiveType(TypeKind.U64), new DataType(null))
                    ),
                    new UserType(
                        "UIntMap",
                        new MapType(
                            new PrimitiveType(TypeKind.UInt), new DataType(null))
                    ),
                    new UserType(
                        "I8Map",
                        new MapType(
                            new PrimitiveType(TypeKind.I8), new DataType(null))
                    ),
                    new UserType(
                        "I16Map",
                        new MapType(
                            new PrimitiveType(TypeKind.I16), new DataType(null))
                    ),
                    new UserType(
                        "I32Map",
                        new MapType(
                            new PrimitiveType(TypeKind.I32), new DataType(null))
                    ),
                    new UserType(
                        "I64Map",
                        new MapType(
                            new PrimitiveType(TypeKind.I64), new DataType(null))
                    ),
                    new UserType(
                        "IntMap",
                        new MapType(
                            new PrimitiveType(TypeKind.Int), new DataType(null))
                    ),
                    new UserType(
                        "BoolMap",
                        new MapType(
                            new PrimitiveType(TypeKind.Bool), new DataType(null))
                    ),
                    new UserType(
                        "StrMap",
                        new MapType(
                            new PrimitiveType(TypeKind.String), new DataType(null))
                    ),
                    new UserType(
                        "EnumMap",
                        new MapType(
                            new EnumType(new EnumValue[] { new EnumValue("KEY", null) }), new DataType(null))
                    )
                );
        }

        [Test]
        public void Key_type_shall_not_be_of_void_type()
        {
            Expect.Schema("type SomeMap map<void><i32>")
                .To_fail_parsing_with<InvalidDataException>();
        }

        [Test]
        public void Key_type_shall_not_be_of_f32_type()
        {
            Expect.Schema("type SomeMap map<f32><i32>")
                .To_fail_parsing_with<InvalidDataException>();
        }

        [Test]
        public void Key_type_shall_not_be_of_f64_type()
        {
            Expect.Schema("type SomeMap map<f64><i32>")
                .To_fail_parsing_with<InvalidDataException>();
        }

        [Test]
        public void Key_type_shall_not_be_of_data_type()
        {
            Expect.Schema("type SomeMap map<data><i32>")
                .To_fail_parsing_with<InvalidDataException>();
        }

        [Test]
        public void Key_type_shall_not_be_of_fixed_data_type()
        {
            Expect.Schema("type SomeMap map<data[16]><i32>")
                .To_fail_parsing_with<InvalidDataException>();
        }
    }
}