using System;
using System.IO;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    [TestFixture]
    public class List
    {
        [Test]
        public void Can_be_defined_with_length_in_brackets()
        {
            Expect.Schema("type SomeList list<str>[4]")
                .To_define_user_types(
                    new UserType("SomeList", new ListType(new PrimitiveType(TypeKind.String), 4)));
        }

        [Test]
        public void Can_be_defined_without_length()
        {
            Expect.Schema("type SomeList list<str>")
                .To_define_user_types(
                    new UserType("SomeList", new ListType(new PrimitiveType(TypeKind.String), null)));
        }

        [Test]
        public void Shall_not_be_defined_without_length_size()
        {
            Expect.Schema("type SomeList list<str>[]")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Length_should_be_at_least_one()
        {
            Expect.Schema("type SomeData list<str>[0]")
                .To_fail_parsing_with<InvalidDataException>();
        }
    }
}