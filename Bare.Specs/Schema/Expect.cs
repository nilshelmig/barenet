using System.IO;
using System.Linq;
using System.Text;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{
    public static class Expect
    {
        public static SchemaTestCase Schema(string schema)
        {
            return new SchemaTestCase(schema);
        }
    }

    public class SchemaTestCase
    {
        private readonly string _schema;

        public SchemaTestCase(string schema)
        {
            _schema = schema;
        }

        public void To_define_user_types(params UserType[] types)
        {
            Assert.AreEqual(types, Parse(_schema));
        }

        public void To_define_no_user_types()
        {
            Assert.AreEqual(new UserType[0], Parse(_schema));
        }

        public void To_fail_parsing_with<T>() where T : System.Exception
        {
            Assert.Throws<T>(() => Parse(_schema));
        }

        private static UserType[] Parse(string schema)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(schema)))
            using (var scanner = new Scanner(new StreamReader(stream, Encoding.UTF8)))
                return Parser.Parse(scanner).ToArray();
        }
    }
}
