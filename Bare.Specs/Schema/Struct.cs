using System;
using System.IO;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs.Schema
{

    [TestFixture]
    public class Struct
    {
        [Test]
        public void Can_be_defined_in_one_line_with_single_field()
        {
            Expect.Schema("type NamedStruct struct { first: i32 }")
                .To_define_user_types(
                    new UserType("NamedStruct", new StructType(new []
                    {
                        new StructField("first", new PrimitiveType(TypeKind.I32))
                    })));
        }

        [Test]
        public void Can_be_defined_in_one_line_with_multiple_fields()
        {
            Expect.Schema("type NamedStruct struct { first: i32 second: u8 third: f32 }")
                .To_define_user_types(
                    new UserType("NamedStruct", new StructType(new []
                    {
                        new StructField("first", new PrimitiveType(TypeKind.I32)),
                        new StructField("second", new PrimitiveType(TypeKind.U8)),
                        new StructField("third", new PrimitiveType(TypeKind.F32))
                    })));
        }

        [Test]
        public void Can_be_defined_in_multiple_lines_with_one_field()
        {
            Expect.Schema(@"
                type NamedStruct struct
                    {
                        first: i32
                    }")
                .To_define_user_types(
                    new UserType("NamedStruct", new StructType(new []
                    {
                        new StructField("first", new PrimitiveType(TypeKind.I32))
                    })));
        }

        [Test]
        public void Can_be_defined_in_multiple_lines_with_multiple_fields()
        {
            Expect.Schema(@"
                type NamedStruct struct
                    {
                        first: i32
                        second: u8
                        third: f32
                    }")
                .To_define_user_types(
                    new UserType("NamedStruct", new StructType(new []
                    {
                        new StructField("first", new PrimitiveType(TypeKind.I32)),
                        new StructField("second", new PrimitiveType(TypeKind.U8)),
                        new StructField("third", new PrimitiveType(TypeKind.F32))
                    })));
        }

        [Test]
        public void Must_have_at_least_one_field()
        {
            Expect.Schema("type NamedStruct struct {}")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Field_shall_not_contain_special_characters()
        {
            Expect.Schema("type NamedStruct struct { Täöüß: i32 }")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Field_shall_not_contain_a_non_letter()
        {
            Expect.Schema("type NamedStruct struct { Number4: i32 }")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Field_shall_not_contain_an_underscore()
        {
            Expect.Schema("type NamedStruct struct { Number_One: i32 }")
                .To_fail_parsing_with<FormatException>();
        }

        [Test]
        public void Field_shall_not_be_of_void_type()
        {
            Expect.Schema("type NamedStruct struct { voidField: void }")
                .To_fail_parsing_with<InvalidDataException>();
        }
    }
}