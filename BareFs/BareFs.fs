﻿namespace BareNET

open System
open System.Text

type Encoding_Result = Result<byte array, string>
type Decoding_Result<'T> = Result<('T * byte array), string>
type Encoder<'T> = 'T -> Encoding_Result
type Decoder<'T> = byte array -> Decoding_Result<'T>

type Union_Case =
  { identifier: uint
    encoded: Encoding_Result
  }

type Union<'T> =
  { encoder: 'T -> Union_Case
    decoder: uint -> 'T
  }

module Bare =
  let inline success (encoder: 'T -> byte array) : Encoder<'T> =
    encoder>>Ok

  let inline andThen (encoder: Encoder<'T>) (value: 'T) (result: Encoding_Result) : Encoding_Result =
    Result.bind (fun data -> value |> encoder |> Result.map (Array.append data)) result

  let inline private tryTake count array =
    try
      Ok (Array.take count array, Array.skip count array)
    with _ ->
      Error "data does not contains needed amount of bytes"

  let inline private mapBytes mapper result =
    result |> Result.map (fun (bytes, rest) -> (mapper bytes, rest))

  let rec private split_value_into_7_bit_groups_least_significant_first (value: uint64) list =
    if value < 128UL
    then
      (byte value &&& 0xFFuy)::list
    else
      split_value_into_7_bit_groups_least_significant_first
        (value>>>7)
        (((byte value &&& 0xFFuy) ||| 0b10000000uy)::list)

  let inline private most_siginifanct_bit_set (value: byte) = (value &&& 0b10000000uy) = 0b10000000uy

  let inline private encode_values (encoder: Encoder<'T>) (values: #seq<'T>) : Encoding_Result =
    values |> Seq.fold
      (fun data value -> data |> Result.bind (fun data -> encoder value |> Result.map (Array.append data)))
      (Ok [||])


  let encode_u8 (value: uint8) : byte array =
    [| value |]

  let decode_u8 (data: byte array) : Decoding_Result<uint8> =
    data
    |> tryTake 1
    |> mapBytes (fun bytes -> bytes.[0])

  let encode_i8 (value: int8) : byte array =
    [| byte value |]

  let decode_i8 (data: byte array) : Decoding_Result<int8> =
    data
    |> tryTake 1
    |> mapBytes (fun bytes -> int8 bytes.[0])

  let encode_u16 (value: uint16) : byte array =
    [| byte value ; byte (value>>>8) |]

  let decode_u16 (data: byte array) : Decoding_Result<uint16> =
    data
    |> tryTake 2
    |> mapBytes (Array.map uint16)
    |> mapBytes (fun bytes -> bytes.[0] ||| (bytes.[1]<<<8))

  let encode_i16 (value: int16) : byte array =
    [| byte value ; byte (value>>>8) |]

  let decode_i16 (data: byte array) : Decoding_Result<int16> =
    data
    |> tryTake 2
    |> mapBytes (Array.map int16)
    |> mapBytes (fun bytes -> bytes.[0] ||| (bytes.[1]<<<8))

  let encode_u32 (value: uint32) : byte array =
    [| byte value ; byte (value>>>8) ; byte (value>>>16) ; byte (value>>>24) |]

  let decode_u32 (data: byte array) : Decoding_Result<uint32> =
    data
    |> tryTake 4
    |> mapBytes (Array.map uint32)
    |> mapBytes (fun bytes -> bytes.[0] ||| (bytes.[1]<<<8) ||| (bytes.[2]<<<16) ||| (bytes.[3]<<<24))

  let encode_i32 (value: int32) : byte array =
    [| byte value ; byte (value>>>8) ; byte (value>>>16) ; byte (value>>>24) |]

  let decode_i32 (data: byte array) : Decoding_Result<int32> =
    data
    |> tryTake 4
    |> mapBytes (Array.map int32)
    |> mapBytes (fun bytes -> bytes.[0] ||| (bytes.[1]<<<8) ||| (bytes.[2]<<<16) ||| (bytes.[3]<<<24))

  let encode_u64 (value: uint64) : byte array =
    [| byte value ; byte (value>>>8) ; byte (value>>>16) ; byte (value>>>24) ; byte (value>>>32) ; byte (value>>>40) ; byte (value>>>48) ; byte (value>>>56) |]

  let decode_u64 (data: byte array) : Decoding_Result<uint64> =
    data
    |> tryTake 8
    |> mapBytes (Array.map uint64)
    |> mapBytes (fun bytes -> bytes.[0] ||| (bytes.[1]<<<8) ||| (bytes.[2]<<<16) ||| (bytes.[3]<<<24) ||| (bytes.[4]<<<32) ||| (bytes.[5]<<<40) ||| (bytes.[6]<<<48) ||| (bytes.[7]<<<56))

  let encode_i64 (value: int64) : byte array =
    [| byte value ; byte (value>>>8) ; byte (value>>>16) ; byte (value>>>24) ; byte (value>>>32) ; byte (value>>>40) ; byte (value>>>48) ; byte (value>>>56) |]

  let decode_i64 (data: byte array) : Decoding_Result<int64> =
    data
    |> tryTake 8
    |> mapBytes (Array.map int64)
    |> mapBytes (fun bytes -> bytes.[0] ||| (bytes.[1]<<<8) ||| (bytes.[2]<<<16) ||| (bytes.[3]<<<24) ||| (bytes.[4]<<<32) ||| (bytes.[5]<<<40) ||| (bytes.[6]<<<48) ||| (bytes.[7]<<<56))

  let encode_uint (value: uint64) : byte array =
    split_value_into_7_bit_groups_least_significant_first value []
    |> List.rev
    |> List.toArray

  let decode_uint (data: byte array) : Decoding_Result<uint64> =
    let rest = data |> Array.skipWhile most_siginifanct_bit_set
    match rest |> Array.tryHead with
    | Some head ->
        data
        |> Seq.takeWhile most_siginifanct_bit_set
        |> fun d -> Seq.append d [ head ]
        |> Seq.map uint64
        |> Seq.indexed
        |> Seq.fold
            (fun value (index, current) -> ((current &&& 0x7FUL) <<< (index*7)) ||| value)
            0UL
        |> fun value -> Ok (value, Array.skip 1 rest)

    | None ->
        Error "wrong data for uint"

  let encode_int (value: int64) : byte array =
    value <<< 1
    |> uint64
    |> fun v -> if value < 0L then ~~~v else v
    |> fun value -> split_value_into_7_bit_groups_least_significant_first value []
    |> List.rev
    |> List.toArray

  let decode_int (data: byte array) : Decoding_Result<int64> =
    match decode_uint data with
    | Ok (v, rest) ->
        v
        |> fun value -> value >>> 1
        |> int64
        |> fun value -> if (v &&& 1UL) = 1UL then ~~~value else value
        |> fun value -> Ok (value, rest)

    | Error _ ->
        Error "wrong data for int"

  let encode_f32 (value : float32) : Encoding_Result =
    if Single.IsNaN value
    then Error "value is not a number"
    else Ok (BitConverter.GetBytes value)

  let decode_f32 (data: byte array) : Decoding_Result<float32> =
    data
    |> tryTake 4
    |> mapBytes (fun bytes -> BitConverter.ToSingle (bytes, 0))

  let encode_f64 (value : float) : Encoding_Result =
    if Double.IsNaN value
    then Error "value is not a number"
    else Ok (BitConverter.GetBytes value)

  let decode_f64 (data: byte array) : Decoding_Result<float> =
    data
    |> tryTake 8
    |> mapBytes (fun bytes -> BitConverter.ToDouble (bytes, 0))

  let encode_bool (value: bool) : byte array =
    [| if value then 1uy else 0uy |]

  let decode_bool (data: byte array) : Decoding_Result<bool> =
    data
    |> tryTake 1
    |> Result.bind (fun (bytes, rest) ->
      match bytes with
      | [| 0uy |] -> Ok (false, rest)
      | [| 1uy |] -> Ok (true, rest)
      | _ -> Error "data does not represent a boolean")

  let encode_enum<'T when 'T: enum<int32>> (value: 'T) : Encoding_Result =
    let enumValue = LanguagePrimitives.EnumToValue value
    if enumValue < 0
    then Error "enum value can't be negative"
    else enumValue |> uint64 |> encode_uint |> Ok

#if FABLE_COMPILER
  let inline decode_enum<'T when ^T: enum<int32>> (data: byte array) : Decoding_Result<'T> =
#else
  let decode_enum<'T when ^T: enum<int32>> (data: byte array) : Decoding_Result<'T> =
#endif
    decode_uint data
    |> mapBytes int32
    |> Result.bind (fun (value, rest) -> if Enum.IsDefined (typeof<'T>, value) then Ok (value, rest) else Error "value is not represented in enum")
    |> mapBytes LanguagePrimitives.EnumOfValue

  let encode_string (value: string) : byte array =
    let bytes = Encoding.UTF8.GetBytes value
    Array.append (bytes.Length |> uint64 |> encode_uint) bytes

  let decode_string (data: byte array) : Decoding_Result<string> =
    decode_uint data
    |> Result.bind (fun (length, rest) -> rest |> tryTake (int32 length))
    |> mapBytes (fun bytes -> Encoding.UTF8.GetString(bytes, 0, bytes.Length))

  let encode_data_fixed_length (length: int32) (value : byte array) : Encoding_Result =
    if length <= 0
    then Error "length must be at least 1"
    else if length <> value.Length
    then Error "data is not the needed length"
    else Ok value

  let decode_data_fixed_length (length: int32) (data : byte array) : Decoding_Result<byte array> =
    data |> tryTake length

  let encode_data (value: byte array) : byte array =
    Array.append (value.Length |> uint64 |> encode_uint) value

  let decode_data (data : byte array) : Decoding_Result<byte array> =
    decode_uint data
    |> Result.bind (fun (length, rest) -> rest |> tryTake (int32 length))

  let encode_optional (encoder: Encoder<'T>) (value: 'T option) : Encoding_Result =
    match value with
    | Some value -> (success encode_bool) true |> andThen encoder value
    | None -> encode_bool false |> Ok

  let decode_optional (decoder: Decoder<'T>) (data: byte array) : Decoding_Result<'T option> =
    match decode_bool data with
    | Ok (false, rest) -> Ok (None, rest)
    | Ok (true, rest) -> decoder rest |> mapBytes Some
    | Error err -> Error err

  let encode_list_fixed_length (length: int32) (encoder: Encoder<'T>) (value: #seq<'T>) : Encoding_Result =
    if length < 1
    then Error "length must be at least 1"
    else if length <> Seq.length value
    then Error "data is not the needed length"
    else encode_values encoder value

  let decode_list_fixed_length (length: int32) (decoder: Decoder<'T>) (data: byte array) : Decoding_Result<'T seq> =
    let rec decode index data elements =
      if index = length
      then Ok (elements, data)
      else
        match decoder data with
        | Error error -> Error error
        | Ok (element, rest) ->
            decode (index + 1) rest (element::elements)
    decode 0 data []
    |> mapBytes (Seq.ofList>>Seq.rev)

  let encode_list (encoder: Encoder<'T>) (value: #seq<'T>) : Encoding_Result =
    (success encode_uint) (Seq.length value |> uint64)
    |> andThen (encode_values encoder) value

  let decode_list (decoder: Decoder<'T>) (data: byte array) : Decoding_Result<'T seq> =
    decode_uint data
    |> Result.bind (fun (length, rest) -> decode_list_fixed_length (int32 length) decoder rest)

  let encode_map (keyEncoder: Encoder<'Key>) (valueEncoder: Encoder<'Value>) (value: Map<'Key, 'Value>) : Encoding_Result =
    (success encode_uint) (uint64 value.Count)
    |> andThen
      (encode_values (fun (key, value) -> keyEncoder key |> Result.bind (fun bytes -> valueEncoder value |> Result.map (Array.append bytes))))
      (Map.toSeq value)

  let decode_map (keyDecoder: Decoder<'Key>) (valueDecoder: Decoder<'Value>) (data: byte array) : Decoding_Result<Map<'Key, 'Value>> =
    let rec decode length index data map =
      if index = length
      then Ok (map, data)
      else
        keyDecoder data
        |> Result.bind (fun (key, rest) -> valueDecoder rest |> mapBytes (fun value -> (key, value)))
        |> function
        | Error error -> Error error
        | Ok ((key, value), rest) ->
            if Map.containsKey key map
            then Error "key is duplicated"
            else
              decode length (index + 1UL) rest (map |> Map.add key value)
    decode_uint data
    |> Result.bind (fun (length, rest) -> decode length 0UL rest Map.empty)

  let void_case identifier : Union_Case =
    { identifier = identifier
      encoded = Ok [||]
    }

  let union_case identifier encoded : Union_Case =
    { identifier = identifier
      encoded = Ok encoded
    }

  let failable_case identifier encoded : Union_Case =
    { identifier = identifier
      encoded = encoded
    }

  let from_value value : Decoder<'T> =
    fun data -> Ok (value, data)

  let error error : Decoder<_> =
    fun _ -> Error error

  let encode_union (resolve: 'TUnion -> Union_Case) (value: 'TUnion) : Encoding_Result =
    resolve value
    |> fun case -> case.encoded |> Result.map (Array.append (case.identifier |> uint64 |> encode_uint))

  let decode_union (decoder: uint -> Decoder<'T>) (data: byte array) : Decoding_Result<'T> =
    decode_uint data
    |> Result.bind (fun (identifier, rest) -> decoder (uint identifier) rest)

  let decode_complex (build: 'A -> 'T) : Decoder<'A -> 'T> =
    fun data -> Ok (build, data)

  let apply (decoder: Decoder<'A>) (complex: Decoder<'A -> 'T>) : Decoder<'T> =
    fun data -> complex data |> Result.bind (fun (func, data) -> decoder data |> mapBytes func)