﻿using System.Collections.Generic;

namespace Bare.CodeGen
{
    internal readonly struct Generator_configuration
    {
        public readonly string Namespace;
        public readonly ListImplementation List_implementation;
        public readonly string Encoding_classname;
        public readonly List<CustomType> Custom_types;
        public readonly int? Indentation_whitespaces;
        public readonly bool Snake_case;

        public static Generator_configuration From_code_configuration(Code_configuration configuration) =>
            new Generator_configuration(
                configuration.Namespace ?? "Bare.Msg",
                configuration.List_type ?? ListImplementation.Array,
                configuration.Encoding_classname ?? "Encoding",
                configuration.Custom_types,
                configuration.Identation_whitespaces,
                configuration.Snake_case);

        private Generator_configuration(string ns, ListImplementation listImplementation, string encodingClassname, List<CustomType> customTypes, int? indentationWhitespaces, bool snakeCase)
        {
            Namespace = ns;
            List_implementation = listImplementation;
            Encoding_classname = encodingClassname;
            Custom_types = customTypes;
            Indentation_whitespaces = indentationWhitespaces;
            Snake_case = snakeCase;
        }

        public int FSharp_indentation => Indentation_whitespaces ?? 2;
    }
}