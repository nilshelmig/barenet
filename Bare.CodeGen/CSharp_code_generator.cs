﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BareNET.Schema;

namespace Bare.CodeGen
{
    internal interface GenType {}
    internal readonly struct GenUnion : GenType
    {
        public readonly string Name;
        public readonly string[] Interfaces;
        public readonly GenUnionCase[] Cases;

        public GenUnion(string name, string[] interfaces, GenUnionCase[] cases)
        {
            Name = name;
            Interfaces = interfaces;
            Cases = cases;
        }
    }

    internal readonly struct GenUnionCase
    {
        public readonly string CSharp_type;
        public readonly uint? Identifier;
        public readonly Func<string, string> Encoder_call;
        public readonly Func<string, string> Decoder_call;

        public GenUnionCase(string csharpType, uint? identifier, Func<string, string> encoderCall, Func<string, string> decoderCall)
        {
            CSharp_type = csharpType;
            Identifier = identifier;
            Encoder_call = encoderCall;
            Decoder_call = decoderCall;
        }
    }

    internal readonly struct GenEnum : GenType
    {
        public readonly string Name;
        public readonly GenEnumValue[] Values;
        public readonly Func<string, string> Encoder_call;
        public readonly Func<string, string> Decoder_call;

        public GenEnum(string name, GenEnumValue[] values, Func<string, string> encoderCall, Func<string, string> decoderCall)
        {
            Name = name;
            Values = values;
            Encoder_call = encoderCall;
            Decoder_call = decoderCall;
        }
    }

    internal readonly struct GenEnumValue
    {
        public readonly string Name;
        public readonly uint? Value;

        public GenEnumValue(string name, uint? value)
        {
            Name = name;
            Value = value;
        }
    }

    internal readonly struct GenStruct : GenType
    {
        public readonly string Name;
        public readonly GenStructField[] Fields;
        public readonly string[] Interfaces;
        public readonly Func<string, string> Encoder_call;
        public readonly Func<string, string> Decoder_call;

        public GenStruct(string name, GenStructField[] fields, string[] interfaces, Func<string, string> encoderCall, Func<string, string> decoderCall)
        {
            Name = name;
            Fields = fields;
            Interfaces = interfaces;
            Encoder_call = encoderCall;
            Decoder_call = decoderCall;
        }
    }

    internal readonly struct GenStructField
    {
        public readonly string Name;
        public readonly string Variable_name;
        public readonly string CSharp_type;
        public readonly ulong? Length;
        public readonly Func<string, string> Encoder_call;
        public readonly Func<string, string> Decoder_call;
        public readonly Func<string, string>? Converter;

        public GenStructField(string name, string variableName, string cSharpType, ulong? length, Func<string, string> encoderCall, Func<string, string> decoderCall, Func<string, string>? converter)
        {
            Name = name;
            Variable_name = variableName;
            CSharp_type = cSharpType;
            Length = length;
            Encoder_call = encoderCall;
            Decoder_call = decoderCall;
            Converter = converter;
        }
    }

    internal readonly struct GenCustomType : GenType
    {
        public readonly string Name;
        public readonly string CSharp_type;
        public readonly Func<string, string> Encoder_call;
        public readonly Func<string, string> Decoder_call;

        public GenCustomType(string name, string cSharp_type, Func<string, string> encoder_call, Func<string, string> decoder_call)
        {
            Name = name;
            CSharp_type = cSharp_type;
            Encoder_call = encoder_call;
            Decoder_call = decoder_call;
        }
    }

    internal enum EncodingKind
    {
        Enum,
        Union,
        Struct,
        Custom
    }

    internal static class CSharp_code_generator
    {
        private static Func<string, string> ListType = type => $"{type}[]";
        private static Func<string, string> ListCollector = arg => $"{arg}.ToArray()";
        private static string Generated_encoding_class = "";
        private static int? Indentation_whitespaces;

        public static string Generate(List<UserType> schema, DateTime generationTime, Generator_configuration configuration)
        {
            switch(configuration.List_implementation)
            {
                case ListImplementation.List:
                {
                    ListType = type => $"List<{type}>";
                    ListCollector = arg => $"{arg}.ToList()";
                    break;
                }
                case ListImplementation.ReadonlyCollection:
                {
                    ListType = type => $"System.Collections.ObjectModel.ReadOnlyCollection<{type}>";
                    ListCollector = arg => $"new System.Collections.ObjectModel.ReadOnlyCollection<int>({arg}.ToList())";
                    break;
                }
            }
            Generated_encoding_class = configuration.Encoding_classname;
            Indentation_whitespaces = configuration.Indentation_whitespaces;

            var builder = new StringBuilder();
            var custom_type_names = configuration.Custom_types.Select(_ => _.Name).ToHashSet();
            var unions_used_for_named_types = Named_types_usages_in_unions(schema, custom_type_names);
            var userTypeEncoding =
                schema
                    .Select(userType => userType.Type switch
                    {
                        EnumType => (userType.Name, custom_type_names.Contains(userType.Name) ? EncodingKind.Custom : EncodingKind.Enum),
                        UnionType => (userType.Name, custom_type_names.Contains(userType.Name) ? EncodingKind.Custom : EncodingKind.Union),
                        _ => (userType.Name, custom_type_names.Contains(userType.Name) ? EncodingKind.Custom : EncodingKind.Struct)
                    })
                    .ToDictionary(_ => _.Name, _ => _.Item2);
            var custom_types_lookup = configuration.Custom_types.ToDictionary(_ => _.Name);
            var types = schema.Aggregate(new Dictionary<string, GenType>(), (state, type) => Apply(state, type, unions_used_for_named_types, userTypeEncoding, custom_types_lookup, configuration.Snake_case), _ => _.Values.ToList());

            Utils.Generate_header(generationTime, builder);
            builder.AppendLine("using System;");
            builder.AppendLine("using System.Linq;");
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine($"namespace {configuration.Namespace}");
            builder.AppendLine("{");
            Generate_types(types, builder);
            Generate_encoding_class(types, builder);
            builder.AppendLine("}");

            return builder.ToString();
        }

        private static IEnumerable<(string namedType, string union)> Named_types_usages_in_union(string baseName, string fieldname, AnyType type, HashSet<string> namedTypes)
        {
            switch (type)
            {
                case ListType listType:
                    switch (listType.Type)
                    {
                        case StructType subStructType:
                        {
                            var name = Next_free_name($"{baseName}_{fieldname}_Struct", namedTypes);
                            namedTypes.Add(name);
                            return Named_types_usages_in_union(name, fieldname, subStructType, namedTypes);
                        }
                        case UnionType unionType:
                        {
                            var name = Next_free_name($"{baseName}_{fieldname}_Union", namedTypes);
                            namedTypes.Add(name);
                            return Named_types_usages_in_union(name, fieldname, unionType, namedTypes);
                        }
                        default:
                            return Named_types_usages_in_union(baseName, fieldname, listType.Type, namedTypes);
                    }
                case MapType mapType:
                    switch (mapType.ValueType)
                    {
                        case StructType subStructType:
                        {
                            var name = Next_free_name($"{baseName}_{fieldname}_Struct", namedTypes);
                            namedTypes.Add(name);
                            return Named_types_usages_in_union(name, fieldname, subStructType, namedTypes);
                        }
                        case UnionType unionType:
                        {
                            var name = Next_free_name($"{baseName}_{fieldname}_Union", namedTypes);
                            namedTypes.Add(name);
                            return Named_types_usages_in_union(name, fieldname, unionType, namedTypes);
                        }
                        default:
                            return Named_types_usages_in_union(baseName, fieldname, mapType.ValueType, namedTypes);
                    }
                case OptionalType optionalType:
                    switch (optionalType.Type)
                    {
                        case StructType subStructType:
                        {
                            var name = Next_free_name($"{baseName}_{fieldname}_Struct", namedTypes);
                            namedTypes.Add(name);
                            return Named_types_usages_in_union(name, fieldname, subStructType, namedTypes);
                        }
                        case UnionType unionType:
                        {
                            var name = Next_free_name($"{baseName}_{fieldname}_Union", namedTypes);
                            namedTypes.Add(name);
                            return Named_types_usages_in_union(name, fieldname, unionType, namedTypes);
                        }
                        default:
                            return Named_types_usages_in_union(baseName, fieldname, optionalType.Type, namedTypes);
                    }
                case StructType structType:
                    return structType.Fields.SelectMany(field =>
                    {
                        switch (field.Type)
                        {
                            case StructType subStructType:
                            {
                                var name = Next_free_name($"{baseName}_{field.Name}_Struct", namedTypes);
                                namedTypes.Add(name);
                                return Named_types_usages_in_union(name, field.Name, subStructType, namedTypes);
                            }
                            case UnionType unionType:
                            {
                                var name = Next_free_name($"{baseName}_{field.Name}_Union", namedTypes);
                                namedTypes.Add(name);
                                return Named_types_usages_in_union(name, field.Name, unionType, namedTypes);
                            }
                            default:
                            {
                                return Named_types_usages_in_union(baseName, field.Name, field.Type, namedTypes);
                            }
                        }
                    });
                case UnionType unionType:
                    return unionType.Members.SelectMany(member =>
                    {
                        switch (member.Type)
                        {
                            case ListType listType:
                            {
                                var name = Next_free_name($"{baseName}_List", namedTypes);
                                namedTypes.Add(name);
                                var anonymousStructType = new StructType(new[] {new StructField("value", listType)});
                                return Named_types_usages_in_union(name, fieldname, anonymousStructType, namedTypes);
                            }
                            case MapType mapType:
                            {
                                var name = Next_free_name($"{baseName}_Map", namedTypes);
                                namedTypes.Add(name);
                                var anonymousStructType = new StructType(new[] {new StructField("value", mapType)});
                                return Named_types_usages_in_union(name, fieldname, anonymousStructType, namedTypes);
                            }
                            case OptionalType optionalType:
                            {
                                var name = Next_free_name($"{baseName}_Option", namedTypes);
                                namedTypes.Add(name);
                                var anonymousStructType =
                                    new StructType(new[] {new StructField("value", optionalType)});
                                return Named_types_usages_in_union(name, fieldname, anonymousStructType, namedTypes);
                            }
                            case StructType structType:
                            {
                                var name = Next_free_name($"{baseName}_Struct", namedTypes);
                                namedTypes.Add(name);
                                return Named_types_usages_in_union(name, fieldname, structType, namedTypes);
                            }
                            case UnionType subUnionType:
                            {
                                var name = Next_free_name($"{baseName}_Union", namedTypes);
                                namedTypes.Add(name);
                                return Named_types_usages_in_union(name, fieldname, subUnionType, namedTypes);
                            }
                            case PrimitiveType:
                                return new (string namedType, string union)[0];
                            case DataType:
                                return new (string namedType, string union)[0];
                            case UserTypeName typeName:
                                return new[] {(typeName.Name, baseName)};
                            default:
                                throw new ArgumentOutOfRangeException(nameof(member.Type), member.Type, null);
                        }
                    });
                default:
                    return new (string namedType, string union)[0];
            }
        }

        private static Dictionary<string, string[]> Named_types_usages_in_unions(List<UserType> schema, HashSet<string> custom_types)
        {
            var usedNames = new HashSet<string>();
            var types_used_in_unions =
                schema
                    .SelectMany(userType => Named_types_usages_in_union(userType.Name, "", userType.Type, usedNames))
                    .ToList();

            var custom_types_used_in_unions =
                types_used_in_unions
                    .Where(_ => custom_types.Contains(_.namedType))
                    .GroupBy(_ => _.namedType)
                    .ToList();
            if (custom_types_used_in_unions.Any())
            {
                var appeareances =
                    string.Join(
                        Environment.NewLine,
                        custom_types_used_in_unions.Select(usage => $"- \"{usage.Key}\" used in {string.Join(", ", usage.Select(_ => $"\"{_.union}\""))}"));
                throw new FormatException($"Custom types in C# can't be used directly in an union.{Environment.NewLine}Provide a named type and replaces them in the used union.{Environment.NewLine}{appeareances}");
            }

            return types_used_in_unions
                .GroupBy(_ => _.namedType)
                .ToDictionary(_ => _.Key, values => values.Select(_ => _.union).ToArray());
        }

        private static Dictionary<string, GenType> Apply(Dictionary<string, GenType> types, UserType userType, Dictionary<string, string[]> interfaces, Dictionary<string, EncodingKind> userTypeEncoding, Dictionary<string, CustomType> custom_types, bool snake_case)
        {
            string[] Interfaces(string name) => interfaces.ContainsKey(name) ? interfaces[name].Select(_ => Utils.Typename(_, snake_case)).ToArray() : new string[0];

            Guard_name_is_already_used(types, userType.Name);
            switch (userType.Type)
            {
                case EnumType type:
                {
                    if (custom_types.ContainsKey(userType.Name))
                    {
                        Register_custom_type(custom_types[userType.Name], types, snake_case);
                    }
                    else
                    {
                        var name = Utils.Typename(userType.Name, snake_case);
                        types.Add(
                            userType.Name,
                            new GenEnum(
                                name,
                                type.Values.Select(v => new GenEnumValue(v.Name, v.Value)).ToArray(),
                                v => $"{Generated_encoding_class}.{name}_Encoded({v})",
                                v => $"{Generated_encoding_class}.Decode_{name}({v})"));
                    }
                    break;
                }

                case UserTypeName { Name: var name } when custom_types.ContainsKey(name):
                {
                    Register_custom_type(custom_types[name], types, snake_case);
                    break;
                }

                case StructType structType:
                {
                    Register_struct(userType.Name, Interfaces(userType.Name), structType.Fields, types, userTypeEncoding, custom_types, snake_case);
                    break;
                }

                case UnionType unionType:
                {
                    Register_union(userType.Name, Interfaces(userType.Name), unionType.Members, types, userTypeEncoding, custom_types, snake_case);
                    break;
                }

                case DataType type:
                {
                    Register_struct(
                        userType.Name,
                        Interfaces(userType.Name),
                        new []{ new StructField("value", type) },
                        types,
                        userTypeEncoding,
                        custom_types,
                        snake_case);
                    break;
                }

                case PrimitiveType type:
                {
                    Register_struct(
                        userType.Name,
                        Interfaces(userType.Name),
                        type.Kind != TypeKind.Void ? new []{ new StructField("value", type) } : new StructField[0],
                        types,
                        userTypeEncoding,
                        custom_types,
                        snake_case);
                    break;
                }
            }

            return types;
        }

        private static void Register_custom_type(CustomType type, Dictionary<string, GenType> types, bool snake_case)
        {
            types.Add(type.Name, new GenCustomType(Utils.Typename(type.Name, snake_case), type.Type, v => $"{type.Encoder}({v})", v => $"{type.Decoder}({v})"));
        }

        private static void Register_struct(string name, string[] interfaces, IEnumerable<StructField> fields, Dictionary<string, GenType> types, Dictionary<string, EncodingKind> userTypeEncoding, Dictionary<string, CustomType> custom_types, bool snake_case)
        {
            var typename = Utils.Typename(name, snake_case);
            types.Add(
                name,
                new GenStruct(
                    typename,
                    fields.Select(field =>
                    {
                        var fieldname = Utils.Typename(field.Name, snake_case);
                        var (cSharpType, encoder, decoder) = Encoder_Decoder_call(typename, fieldname, field.Type, types, userTypeEncoding, custom_types, snake_case);
                        return new GenStructField(
                            fieldname,
                            Utils.As_lowercase_name(fieldname),
                            cSharpType,
                            field.Type switch { ListType a => a.Length, DataType p => p.Length, _ => null },
                            encoder,
                            decoder,
                            field.Type is ListType ? ListCollector : null);
                    }).ToArray(),
                    interfaces,
                    v => $"{v}.Encoded()",
                    v => $"{typename}.Decode({v})"));
        }

        private static string Subname_of(string name) => name.Contains('.') ? name.Split('.').First() : name;

        private static (string type, Func<string, string> encoder, Func<string, string> decoder) Encoder_Decoder_call(string name, string fieldname, AnyType type, Dictionary<string, GenType> types, Dictionary<string, EncodingKind> userTypeEncoding, Dictionary<string, CustomType> custom_types, bool snake_case)
        {
            switch (type)
            {
                case PrimitiveType primitiveType:
                {
                    return (TypeKind_as_CSharp_type(primitiveType.Kind), v => PrimitiveEncoderCall(v, primitiveType), v => PrimitiveDecoderCall(v, primitiveType));
                }
                case DataType dataType:
                {
                    return ("byte[]", v => DataEncoderCall(v, dataType), v => DataDecoderCall(v, dataType));
                }
                case UnionType unionType:
                {
                    var unionName =
                        Next_free_name(
                            snake_case
                                ? $"{name}_{fieldname}_Union"
                                : $"{name}{fieldname}Union",
                            types.Keys.ToHashSet());
                    Register_union(unionName, new string[0], unionType.Members, types, userTypeEncoding, custom_types, snake_case);
                    return (unionName, v => $"{Generated_encoding_class}.{unionName}_Encoded({v})", v => $"{Generated_encoding_class}.Decode_{unionName}({v})");
                }
                case StructType structType:
                {
                    var structName =
                        Next_free_name(
                            snake_case
                                ? $"{name}_{fieldname}_Struct"
                                : $"{name}{fieldname}Struct",
                            types.Keys.ToHashSet());
                    Register_struct(structName, new string[0], structType.Fields, types, userTypeEncoding, custom_types, snake_case);
                    return (structName, v => $"{v}.Encoded()", v => $"{structName}.Decode({v})");
                }
                case ListType listType:
                {
                    var (cSharpType, encoder, decoder) = Encoder_Decoder_call(name, fieldname, listType.Type, types, userTypeEncoding, custom_types, snake_case);
                    return
                        listType.Length.HasValue
                            ? (ListType(cSharpType),
                                v => $"{Utils.BareClass}.Encode_list_fixed_length({listType.Length.Value}, {v}, {Subname_of(v)}List => {encoder($"{Subname_of(v)}List")})",
                                v => $"{Utils.BareClass}.Decode_list_fixed_length({listType.Length.Value}, {v}, {Subname_of(v)}List => {decoder($"{Subname_of(v)}List")})")
                            : (ListType(cSharpType),
                                v => $"{Utils.BareClass}.Encode_list({v}, {Subname_of(v)}List => {encoder($"{Subname_of(v)}List")})",
                                v => $"{Utils.BareClass}.Decode_list({v}, {Subname_of(v)}List => {decoder($"{Subname_of(v)}List")})");
                }
                case MapType mapType:
                {
                    if (mapType.KeyType is PrimitiveType keyType && keyType.Kind != TypeKind.Void)
                    {
                        var (valueCSharpType, valueEncoder, valueDecoder) = Encoder_Decoder_call(name, fieldname, mapType.ValueType, types, userTypeEncoding, custom_types, snake_case);
                        return (
                            $"Dictionary<{TypeKind_as_CSharp_type(keyType.Kind)},{valueCSharpType}>",
                            v => $"{Utils.BareClass}.Encode_map({v}, {Subname_of(v)}Key => {PrimitiveEncoderCall($"{Subname_of(v)}Key", keyType)}, {Subname_of(v)}Value => {valueEncoder($"{Subname_of(v)}Value")})",
                            v => $"{Utils.BareClass}.Decode_map({v}, {Subname_of(v)}Key => {PrimitiveDecoderCall($"{Subname_of(v)}Key", keyType)}, {Subname_of(v)}Value => {valueDecoder($"{Subname_of(v)}Value")})"
                        );
                    }

                    throw new FormatException($"Type {mapType.KeyType} can't be used as key type for a map. Only primitive types which is not void can be used");
                }
                case OptionalType optionalType:
                {
                    if (optionalType.Type is OptionalType) throw new FormatException("optional<optional<>> is not supported");

                    var (valueCSharpType, encoder, decoder) = Encoder_Decoder_call(name, fieldname, optionalType.Type, types, userTypeEncoding, custom_types, snake_case);
                    return Is_reference_type(optionalType.Type, userTypeEncoding)
                        ? ( valueCSharpType,
                            v => $"{Utils.BareClass}.Encode_optional_ref({v}, {Subname_of(v)}Opt => {encoder($"{Subname_of(v)}Opt")})",
                            v => $"{Utils.BareClass}.Decode_optional_ref({v}, {Subname_of(v)}Opt => {decoder($"{Subname_of(v)}Opt")})")
                        : ($"{valueCSharpType}?",
                            v => $"{Utils.BareClass}.Encode_optional<{valueCSharpType}>({v}, {Subname_of(v)}Opt => {encoder($"{Subname_of(v)}Opt")})",
                            v => $"{Utils.BareClass}.Decode_optional({v}, {Subname_of(v)}Opt => {decoder($"{Subname_of(v)}Opt")})");
                }
                case UserTypeName userType:
                {
                    if (custom_types.ContainsKey(userType.Name))
                    {
                        var (_, encoder_call, decoder_call) = UserType_Encoder_Decoder_Call(EncodingKind.Custom, userType, snake_case);
                        return (custom_types[userType.Name].Type, encoder_call, decoder_call);
                    }

                    if (!userTypeEncoding.ContainsKey(userType.Name)) throw new FormatException($"Type {userType.Name} does not exists");
                    var kind = userTypeEncoding[userType.Name];
                    var (typename, encoder, decoder) = UserType_Encoder_Decoder_Call(kind, userType, snake_case);
                    return (typename, encoder, decoder);
                }
                default: throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private static (string, Func<string, string> encoder, Func<string, string> decoder) UserType_Encoder_Decoder_Call(EncodingKind kind, UserTypeName userType, bool snake_case)
        {
            var name = Utils.Typename(userType.Name, snake_case);
            return kind == EncodingKind.Enum || kind == EncodingKind.Union || kind == EncodingKind.Custom
                ? (name,
                    v => $"{Generated_encoding_class}.{name}_Encoded({v})",
                    v => $"{Generated_encoding_class}.Decode_{name}({v})")
                : (name,
                    v => $"{v}.Encoded()",
                    v => $"{name}.Decode({v})");
        }

        private static string Next_free_name(string basename, HashSet<string> types)
        {
            if (!types.Contains(basename)) return basename;

            var suffix = 2;
            while (true)
            {
                var name = $"{basename}{suffix}";
                if (!types.Contains(name)) return name;
                suffix++;
            }
        }

        private static void Register_union(string name, string[] interfaces, IEnumerable<UnionMember> members,
            Dictionary<string, GenType> types,
            Dictionary<string, EncodingKind> userTypeEncoding,
            Dictionary<string, CustomType> custom_types,
            bool snake_case)
        {
            var unionname = Utils.Typename(name, snake_case);
            types.Add(
                name,
                new GenUnion(
                    unionname,
                    interfaces,
                    members
                        .Union_members_with_identifier()
                        .Select(identified_member =>
                        {
                            var (identifier, member) = identified_member;
                            switch (member.Type)
                            {
                                case PrimitiveType primitiveType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_{primitiveType.Kind}_case_{identifier}"
                                                : $"{unionname}{primitiveType.Kind}Case{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_struct(
                                        caseName,
                                        new []{ unionname },
                                        primitiveType.Kind != TypeKind.Void ? new[] { new StructField("value", primitiveType) } : new StructField[0],
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                }
                                case DataType dataType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_Data_case_{identifier}"
                                                : $"{unionname}DataCase{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_struct(
                                        caseName,
                                        new []{ unionname },
                                        new[] { new StructField("value", dataType) },
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                }
                                case ListType listType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_List_case_{identifier}"
                                                : $"{unionname}ListCase{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_struct(
                                        caseName,
                                        new[] { unionname },
                                        new[] { new StructField("value", listType) },
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                }
                                case MapType mapType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_Map_case_{identifier}"
                                                : $"{unionname}MapCase{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_struct(
                                        caseName,
                                        new[] { unionname },
                                        new[] { new StructField("value", mapType) },
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                }
                                case OptionalType optionalType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_Option_case_{identifier}"
                                                : $"{unionname}OptionCase{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_struct(
                                        caseName,
                                        new[] { unionname },
                                        new[] { new StructField("value", optionalType) },
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                }
                                case StructType structType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_Struct_case_{identifier}"
                                                : $"{unionname}StructCase{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_struct(
                                        caseName,
                                        new[] { unionname },
                                        structType.Fields,
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                }
                                case UnionType unionType:
                                {
                                    var caseName =
                                        Next_free_name(
                                            snake_case
                                                ? $"{unionname}_Union_case_{identifier}"
                                                : $"{unionname}UnionCase{identifier}",
                                            types.Keys.ToHashSet());
                                    Register_union(
                                        caseName,
                                        new[] { unionname },
                                        unionType.Members,
                                        types,
                                        userTypeEncoding,
                                        custom_types,
                                        snake_case);
                                    return new GenUnionCase(
                                        caseName,
                                        member.Identifier,
                                        v => $"{Generated_encoding_class}.{caseName}_Encoded({v})",
                                        v => $"{Generated_encoding_class}.Decode_{caseName}({v})");
                                }
                                case UserTypeName userType:
                                {
                                    if (!userTypeEncoding.ContainsKey(userType.Name)) throw new FormatException($"Type {userType.Name} does not exists");
                                    var kind = userTypeEncoding[userType.Name];

                                    if (kind == EncodingKind.Enum)
                                    {
                                        var caseName =
                                            Next_free_name(
                                                snake_case
                                                    ? $"{unionname}_{Utils.To_snake_case(userType.Name)}"
                                                    : $"{unionname}{userType.Name}",
                                                types.Keys.ToHashSet());
                                        Register_struct(
                                            caseName,
                                            new[] { unionname },
                                            new[] { new StructField("value", userType) },
                                            types,
                                            userTypeEncoding,
                                            custom_types,
                                            snake_case);
                                        return new GenUnionCase(caseName, member.Identifier, v => $"{v}.Encoded()", v => $"{caseName}.Decode({v})");
                                    }

                                    var (typename, encoder, decoder) = UserType_Encoder_Decoder_Call(kind, userType, snake_case);
                                    return new GenUnionCase(typename, member.Identifier, encoder, decoder);
                                }
                                default:
                                    throw new ArgumentOutOfRangeException(nameof(member.Type), member.Type, null);
                            }
                        }).ToArray()));
        }

        private static void Guard_name_is_already_used(Dictionary<string, GenType> types, string name)
        {
            if (types.ContainsKey(name)) throw new FormatException($"Type {name} is already defined");
        }

        private static void Generate_types(IEnumerable<GenType> types, StringBuilder builder)
        {
            var generated_types =
                types
                    .Select(userType =>
                        userType switch
                        {
                            GenEnum type => Generate_enum_type(type),
                            GenUnion type => Generate_union(type),
                            GenStruct type => Generate_struct(type),
                            GenCustomType => null,
                            _ => throw new ArgumentOutOfRangeException(nameof(userType), userType, null)
                        })
                    .Where(_ => _ != null)
                    .Select(code => code.With_indentation(1, Indentation_whitespaces))
                    .ToList();
            if (!generated_types.Any()) return;

            builder.AppendJoin(
                $"{Environment.NewLine}{Environment.NewLine}",
                generated_types);
            builder.AppendLine();
        }

        private static string Generate_enum_type(GenEnum type)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"public enum {type.Name}");
            builder.AppendLine("{");
            builder.AppendLine(string.Join($",{Environment.NewLine}", type.Values.Select(Generate_enum_value)).With_indentation(1, Indentation_whitespaces));
            builder.Append("}");
            return builder.ToString();
        }

        private static string Generate_enum_value(GenEnumValue value)
        {
            return value.Value.HasValue
                ? $"{value.Name} = {value.Value.Value}"
                : value.Name;
        }

        private static string TypeKind_as_CSharp_type(TypeKind kind)
        {
            return kind switch
            {
                TypeKind.UInt => "ulong",
                TypeKind.U8 => "byte",
                TypeKind.U16 => "ushort",
                TypeKind.U32 => "uint",
                TypeKind.U64 => "ulong",
                TypeKind.Int => "long",
                TypeKind.I8 => "sbyte",
                TypeKind.I16 => "short",
                TypeKind.I32 => "int",
                TypeKind.I64 => "long",
                TypeKind.F32 => "float",
                TypeKind.F64 => "double",
                TypeKind.Bool => "bool",
                TypeKind.String => "string",
                TypeKind.Void => "", // result into struct without fields
                _ => throw new ArgumentOutOfRangeException(nameof(kind), kind, null)
            };
        }

        private static string Generate_struct(GenStruct type)
        {
            var builder = new StringBuilder();
            var interfaceCode =
                type.Interfaces.Any()
                    ? $" : {string.Join(", ", type.Interfaces)}"
                    : "";

            if (type.Fields.Length == 0)
            {
                builder.AppendLine($"public readonly struct {type.Name}{interfaceCode}");
                builder.AppendLine("{");
                builder.AppendLine("public byte[] Encoded() { return new byte[0]; }".With_indentation(1, Indentation_whitespaces));
                builder.AppendLine($"public static {type.Name} Decoded(byte[] data) {{ return new {type.Name}(); }}".With_indentation(1, Indentation_whitespaces));
                builder.AppendLine($"public static ValueTuple<{type.Name}, byte[]> Decode(byte[] data) {{ return new ValueTuple<{type.Name}, byte[]>(new {type.Name}(), data); }}".With_indentation(1, Indentation_whitespaces));
                builder.Append("}");
                return builder.ToString();
            }

            builder.AppendLine($"public readonly struct {type.Name}{interfaceCode}");
            builder.AppendLine("{");

            foreach (var field in type.Fields) builder.AppendLine($"public readonly {field.CSharp_type} {field.Name};".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine();

            var arguments = string.Join(", ", type.Fields.Select(_ => $"{_.CSharp_type} {_.Variable_name}"));
            builder.AppendLine($"public {type.Name}({arguments})".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("{".With_indentation(1, Indentation_whitespaces));
            foreach (var field in type.Fields)
            {
                if (field.Length.HasValue)
                {
                    builder.AppendLine($"if ({field.Variable_name}.Length != {field.Length.Value}) throw new ArgumentException(\"Length of list must be {field.Length.Value}\", nameof({field.Variable_name}));".With_indentation(2, Indentation_whitespaces));
                }
                builder.AppendLine($"{field.Name} = {field.Variable_name};".With_indentation(2, Indentation_whitespaces));
            }
            builder.AppendLine("}".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine();

            builder.AppendLine("public byte[] Encoded()".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("{".With_indentation(1, Indentation_whitespaces));
            if (type.Fields.Length == 1)
            {
                builder.AppendLine($"return {type.Fields.First().Encoder_call(type.Fields.First().Name)};".With_indentation(2, Indentation_whitespaces));
            }
            else
            {
                builder.AppendLine($"return {type.Fields.First().Encoder_call(type.Fields.First().Name)}".With_indentation(2, Indentation_whitespaces));
                foreach (var field in type.Fields.Skip(1)) builder.AppendLine($".Concat({field.Encoder_call(field.Name)})".With_indentation(3, Indentation_whitespaces));
                builder.AppendLine(".ToArray();".With_indentation(3, Indentation_whitespaces));
            }
            builder.AppendLine("}".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine();

            builder.AppendLine($"public static {type.Name} Decoded(byte[] data) {{ return Decode(data).Item1; }}".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine();

            builder.AppendLine($"public static ValueTuple<{type.Name}, byte[]> Decode(byte[] data)".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("{".With_indentation(1, Indentation_whitespaces));

            var dataName = "data";
            foreach (var field in type.Fields)
            {
                var decoder = field.Decoder_call(dataName);
                builder.AppendLine($"var {field.Variable_name} = {decoder};".With_indentation(2, Indentation_whitespaces));
                dataName = $"{field.Variable_name}.Item2";
            }

            builder.AppendLine($"return new ValueTuple<{type.Name}, byte[]>(".With_indentation(2, Indentation_whitespaces));
            builder.AppendLine($"new {type.Name}({string.Join(", ", type.Fields.Select(_ => _.Converter != null ? _.Converter($"{_.Variable_name}.Item1") : $"{_.Variable_name}.Item1"))}),".With_indentation(3, Indentation_whitespaces));
            builder.AppendLine(
                type.Fields.Length == 0
                    ? "new byte[0]);".With_indentation(3, Indentation_whitespaces)
                    : $"{type.Fields.Last().Variable_name}.Item2);".With_indentation(3, Indentation_whitespaces));
            builder.AppendLine("}".With_indentation(1, Indentation_whitespaces));

            builder.Append("}");
            return builder.ToString();
        }

        private static string PrimitiveEncoderCall(string variableName, PrimitiveType primitiveType)
        {
            return primitiveType.Kind switch
            {
                TypeKind.Int => $"{Utils.BareClass}.Encode_int({variableName})",
                TypeKind.I8 => $"{Utils.BareClass}.Encode_i8({variableName})",
                TypeKind.I16 => $"{Utils.BareClass}.Encode_i16({variableName})",
                TypeKind.I32 => $"{Utils.BareClass}.Encode_i32({variableName})",
                TypeKind.I64 => $"{Utils.BareClass}.Encode_i64({variableName})",
                TypeKind.UInt => $"{Utils.BareClass}.Encode_uint({variableName})",
                TypeKind.U8 => $"{Utils.BareClass}.Encode_u8({variableName})",
                TypeKind.U16 => $"{Utils.BareClass}.Encode_u16({variableName})",
                TypeKind.U32 => $"{Utils.BareClass}.Encode_u32({variableName})",
                TypeKind.U64 => $"{Utils.BareClass}.Encode_u64({variableName})",
                TypeKind.F32 => $"{Utils.BareClass}.Encode_f32({variableName})",
                TypeKind.F64 => $"{Utils.BareClass}.Encode_f64({variableName})",
                TypeKind.Bool => $"{Utils.BareClass}.Encode_bool({variableName})",
                TypeKind.String => $"{Utils.BareClass}.Encode_string({variableName})",
                TypeKind.Void => "new byte[0]",
                _ => throw new ArgumentOutOfRangeException(nameof(primitiveType.Kind), primitiveType.Kind, null),
            };
        }

        private static string PrimitiveDecoderCall(string dataName, PrimitiveType primitiveType)
        {
            return primitiveType.Kind switch
            {
                TypeKind.Int => $"{Utils.BareClass}.Decode_int({dataName})",
                TypeKind.I8 => $"{Utils.BareClass}.Decode_i8({dataName})",
                TypeKind.I16 => $"{Utils.BareClass}.Decode_i16({dataName})",
                TypeKind.I32 => $"{Utils.BareClass}.Decode_i32({dataName})",
                TypeKind.I64 => $"{Utils.BareClass}.Decode_i64({dataName})",
                TypeKind.UInt => $"{Utils.BareClass}.Decode_uint({dataName})",
                TypeKind.U8 => $"{Utils.BareClass}.Decode_u8({dataName})",
                TypeKind.U16 => $"{Utils.BareClass}.Decode_u16({dataName})",
                TypeKind.U32 => $"{Utils.BareClass}.Decode_u32({dataName})",
                TypeKind.U64 => $"{Utils.BareClass}.Decode_u64({dataName})",
                TypeKind.F32 => $"{Utils.BareClass}.Decode_f32({dataName})",
                TypeKind.F64 => $"{Utils.BareClass}.Decode_f64({dataName})",
                TypeKind.Bool => $"{Utils.BareClass}.Decode_bool({dataName})",
                TypeKind.String => $"{Utils.BareClass}.Decode_string({dataName})",
                TypeKind.Void => "",
                _ => throw new ArgumentOutOfRangeException(nameof(primitiveType.Kind), primitiveType.Kind, null)
            };
        }

        private static string DataEncoderCall(string variableName, DataType type)
        {
            return type.Length.HasValue
                ? $"{Utils.BareClass}.Encode_data_fixed_length({type.Length.Value}, {variableName})"
                : $"{Utils.BareClass}.Encode_data({variableName})";
        }

        private static string DataDecoderCall(string dataName, DataType type)
        {
            return type.Length.HasValue
                ? $"{Utils.BareClass}.Decode_data_fixed_length({type.Length.Value}, {dataName})"
                : $"{Utils.BareClass}.Decode_data({dataName})";
        }

        private static bool Is_reference_type(AnyType type, Dictionary<string, EncodingKind> userTypes)
        {
            return type switch
            {
                EnumType => false,
                BareNET.Schema.ListType => true,
                MapType => true,
                OptionalType => false,
                PrimitiveType primitiveType =>
                    primitiveType.Kind switch
                    {
                        TypeKind.Int => false,
                        TypeKind.I8 => false,
                        TypeKind.I16 => false,
                        TypeKind.I32 => false,
                        TypeKind.I64 => false,
                        TypeKind.UInt => false,
                        TypeKind.U8 => false,
                        TypeKind.U16 => false,
                        TypeKind.U32 => false,
                        TypeKind.U64 => false,
                        TypeKind.F32 => false,
                        TypeKind.F64 => false,
                        TypeKind.Bool => false,
                        TypeKind.String => true,
                        TypeKind.Void => false,
                        _ => throw new ArgumentOutOfRangeException(nameof(primitiveType.Kind), primitiveType.Kind, null)
                    },
                DataType => true,
                StructType => false,
                UnionType => true,
                UserTypeName userType => userTypes[userType.Name] == EncodingKind.Union,
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }

        private static string Generate_union(GenUnion type)
        {
            var builder = new StringBuilder();
            var interfaceCode =
                type.Interfaces.Any()
                    ? $" : {string.Join(", ", type.Interfaces)}"
                    : "";
            builder.AppendFormat("public interface {0}{1} {{ /* Base type of union */ }}", type.Name, interfaceCode);
            return builder.ToString();
        }

        private static void Generate_encoding_class(IEnumerable<GenType> types, StringBuilder builder)
        {
            var encoding_methods =
                types
                    .Select(userType =>
                        userType switch
                        {
                            GenEnum type => Enum_encoding_methods(type),
                            GenUnion type => Union_encoding_methods(type),
                            GenCustomType type => Custom_type_encoding_methods(type),
                            _ => null
                        })
                    .Where(_ => _ != null)
                    .Select(_ => _!.With_indentation(2, Indentation_whitespaces))
                    .ToList();
            if (!encoding_methods.Any()) return;

            builder.AppendLine();
            builder.AppendLine($"public static class {Generated_encoding_class}".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("{".With_indentation(1, Indentation_whitespaces));

            builder.AppendJoin($"{Environment.NewLine}{Environment.NewLine}{Environment.NewLine}", encoding_methods);

            builder.AppendLine();
            builder.AppendLine("}".With_indentation(1, Indentation_whitespaces));
        }

        private static string Enum_encoding_methods(GenEnum type)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"public static byte[] {type.Name}_Encoded({type.Name} value)");
            builder.AppendLine("{");
            builder.AppendLine($"return {Utils.BareClass}.Encode_enum(value);".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("}");
            builder.AppendLine();
            builder.AppendLine($"public static {type.Name} {type.Name}_Decoded(byte[] data)");
            builder.AppendLine("{");
            builder.AppendLine($"return Decode_{type.Name}(data).Item1;".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("}");
            builder.AppendLine();
            builder.AppendLine($"public static ValueTuple<{type.Name}, byte[]> Decode_{type.Name}(byte[] data)");
            builder.AppendLine("{");
            builder.AppendLine($"return {Utils.BareClass}.Decode_enum<{type.Name}>(data);".With_indentation(1, Indentation_whitespaces));
            builder.Append("}");
            return builder.ToString();
        }

        private static string Union_encoding_methods(GenUnion type)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"private static readonly BareNET.Union<{type.Name}> _{type.Name} =");
            builder.AppendLine($"BareNET.Union<{type.Name}>.Register()".With_indentation(1, Indentation_whitespaces));
            builder.AppendJoin(Environment.NewLine, type.Cases.Select(unionCase => Union_case_register_code(unionCase, type.Name).With_indentation(2, Indentation_whitespaces)));
            builder.AppendLine(";");
            builder.AppendLine();
            builder.AppendLine($"public static byte[] {type.Name}_Encoded({type.Name} value)");
            builder.AppendLine("{");
            builder.AppendLine($"return {Utils.BareClass}.Encode_union(value, _{type.Name});".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("}");
            builder.AppendLine();
            builder.AppendLine($"public static {type.Name} {type.Name}_Decoded(byte[] data)");
            builder.AppendLine("{");
            builder.AppendLine($"return Decode_{type.Name}(data).Item1;".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("}");
            builder.AppendLine();
            builder.AppendLine($"public static ValueTuple<{type.Name}, byte[]> Decode_{type.Name}(byte[] data)");
            builder.AppendLine("{");
            builder.AppendLine($"return {Utils.BareClass}.Decode_union<{type.Name}>(data, _{type.Name});".With_indentation(1, Indentation_whitespaces));
            builder.Append("}");

            return builder.ToString();
        }

        private static string Union_case_register_code(GenUnionCase unionCase, string unionCSharpType)
        {
            var method =
                unionCase.Identifier.HasValue
                    ? $".With_Case_identified_by<{unionCase.CSharp_type}>({unionCase.Identifier.Value}, "
                    : $".With_Case<{unionCase.CSharp_type}>(";
            var encoder = $"v => {unionCase.Encoder_call($"(({unionCase.CSharp_type}) v)")}";
            var decoder = $"d => {{ var decoded = {unionCase.Decoder_call("d")}; return new ValueTuple<{unionCSharpType}, byte[]>(decoded.Item1, decoded.Item2); }}";
            return $"{method}{encoder}, {decoder})";
        }

        private static string Custom_type_encoding_methods(GenCustomType type)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"public static byte[] {type.Name}_Encoded({type.CSharp_type} value)");
            builder.AppendLine("{");
            builder.AppendLine($"return {type.Encoder_call("value")};".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("}");
            builder.AppendLine();
            builder.AppendLine($"public static {type.CSharp_type} {type.Name}_Decoded(byte[] data)");
            builder.AppendLine("{");
            builder.AppendLine($"return Decode_{type.Name}(data).Item1;".With_indentation(1, Indentation_whitespaces));
            builder.AppendLine("}");
            builder.AppendLine();
            builder.AppendLine($"public static ValueTuple<{type.CSharp_type}, byte[]> Decode_{type.Name}(byte[] data)");
            builder.AppendLine("{");
            builder.AppendLine($"return {type.Decoder_call("data")};".With_indentation(1, Indentation_whitespaces));
            builder.Append("}");
            return builder.ToString();
        }
    }
}