using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BareNET.Schema;

namespace Bare.CodeGen
{
    internal static class Utils
    {
        internal const string BareClass = "BareNET.Bare";

        internal static void Generate_header(DateTime generationTime, StringBuilder builder)
        {
            builder.AppendLine("//////////////////////////////////////////////////");
            builder.AppendLine($"// Generated code by BareNET - {generationTime:g} //");
            builder.AppendLine("//////////////////////////////////////////////////");
        }

        internal static string Typename(string name, bool snake_case) => snake_case ? As_uppercase_name(To_snake_case(name)) : As_uppercase_name(name);

        internal static string To_snake_case(string text)
        {
            if (text.Length == 0) return text;

            var origin = text.ToCharArray().Reverse().ToArray();
            var parts =
                origin
                    .Skip(1)
                    .Aggregate((origin.First(), new StringBuilder(origin.First().ToString()), new List<string>()),
                        (state, c) =>
                        {
                            var (lastChar, current, result) = state;
                            if (current.Length > 0 && char.IsUpper(lastChar) && char.IsLower(c))
                            {
                                result.Add(current.ToString());
                                current.Clear();
                                current.Append(c);
                                return (c, current, result);
                            }
                            
                            current.Append(c);
                            if (char.IsLower(lastChar) && char.IsUpper(c))
                            {
                                result.Add(current.ToString());
                                current.Clear();
                            }
                            return (c, current, result);
                        },
                        state => state.Item2.Length > 0 ? state.Item3.Append(state.Item2.ToString()) : state.Item3)
                    .Reverse()
                    .Select(_ => new string(_.Reverse().ToArray()))
                    .Select(str => str.Length >= 2 && char.IsUpper(str[0]) && (char.IsUpper(str[1]) || char.IsNumber(str[1])) ? str : str.ToLower());

            return string.Join('_', parts);
        }

        internal static string As_uppercase_name(string name) => $"{name[..1].ToUpper()}{name[1..]}";
        internal static string As_lowercase_name(string name) => $"{name[..1].ToLower()}{name[1..]}";

        internal static string With_indentation(this string code, int level, int? spaces)
        {
            var builder = new StringBuilder();
            var lines = code.Split(Environment.NewLine).ToArray();
            var tab = spaces.HasValue ? new string(' ', level * spaces.Value) : new string('\t', level);
            if (lines.Length == 1)
            {
                builder.AppendFormat("{0}{1}", tab, lines.First());
            }
            else
            {
                for (var i = 0; i < lines.Length; i++)
                {
                    builder.AppendFormat("{0}{1}", tab, lines[i]);
                    if (i < lines.Length-1) builder.AppendLine();
                }
            }
            return builder.ToString();
        }

        internal static IEnumerable<(uint, UnionMember)> Union_members_with_identifier(this IEnumerable<UnionMember> union_members) =>
            union_members
                .Aggregate(
                    (0u, new List<(uint, UnionMember)>()),
                    (state, member) =>
                    {
                        var (identifier, members) = state;
                        if (member.Identifier.HasValue)
                        {
                            members.Add((member.Identifier.Value, member));
                            return (member.Identifier.Value, members);
                        }

                        members.Add((identifier++, member));
                        return (identifier, members);
                    },
                    state => state.Item2);
    }
}