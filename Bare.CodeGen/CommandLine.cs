#nullable enable
using System;
using System.Linq;
using System.Collections.Generic;

namespace Bare.CodeGen
{
    internal enum TargetLanguage { CSharp, FSharp }

    internal enum ListImplementation
    {
        List,
        Array,
        ReadonlyCollection
    }

    internal readonly struct CustomType
    {
        public readonly string Name;
        public readonly string Type;
        public readonly string Encoder;
        public readonly string Decoder;

        public CustomType(string name, string type, string encoder, string decoder)
        {
            Name = name;
            Type = type;
            Encoder = encoder;
            Decoder = decoder;
        }
    }

    internal readonly struct Code_configuration
    {
        public readonly TargetLanguage? Target_language;
        public readonly string? Namespace;
        public readonly ListImplementation? List_type;
        public readonly string? Encoding_classname;
        public readonly List<CustomType> Custom_types;
        public readonly int? Identation_whitespaces;
        public readonly bool Snake_case;

        public Code_configuration(TargetLanguage? targetLanguage, string? @namespace, ListImplementation? list_type, string? encoding_classname, List<CustomType> customTypes, int? identation_whitespaces, bool snakeCase)
        {
            Target_language = targetLanguage;
            Namespace = @namespace;
            List_type = list_type;
            Encoding_classname = encoding_classname;
            Custom_types = customTypes;
            Identation_whitespaces = identation_whitespaces;
            Snake_case = snakeCase;
        }
    }

    internal interface Command {}

    internal readonly struct Generate_from_File : Command
    {
        public readonly string Path_to_schema;
        public readonly string Output_path;
        public readonly Code_configuration Configuration;

        public Generate_from_File(string path_to_schema, string output_path, Code_configuration configuration)
        {
            Path_to_schema = path_to_schema;
            Output_path = output_path;
            Configuration = configuration;
        }
    }

    internal readonly struct Generate_from_StdIn_to_StdOut : Command
    {
        public readonly Code_configuration Configuration;

        public Generate_from_StdIn_to_StdOut(Code_configuration configuration)
        {
            Configuration = configuration;
        }
    }

    internal readonly struct Print_help : Command {}

    internal readonly struct Wrong_Arguments : Command
    {
        public readonly string Message;

        public Wrong_Arguments(string message)
        {
            Message = message;
        }
    }

    internal readonly struct Internal_Error : Command
    {
        public readonly Exception Exception;

        public Internal_Error(Exception exception)
        {
            Exception = exception;
        }
    }


    internal enum Argument_Collector
    {
        None,
        Language,
        List_Type,
        Namespace,
        Classname,
        Identation_whitespaces,
        Custom_Type_Name,
        Custom_Type_Type,
        Custom_Type_Encoder,
        Custom_Type_Decoder
    }

    internal struct Argument_Holder
    {
        public string? Path_to_schema;
        public string? Output_path;
        public bool StdIn;
        public TargetLanguage? Language;
        public ListImplementation? List_type;
        public string? Namespace;
        public string? Encoding_classname;
        public List<CustomType> CustomTypes;
        public string CustomType_Name;
        public string CustomType_Type;
        public string CustomType_Encoder;
        public int? Identation_whitespaces;
        public bool Snake_case;
        public bool Help;

        public static Argument_Holder Empty() =>
            new Argument_Holder(
                null,
                null,
                false,
                null,
                null,
                null,
                null,
                new List<CustomType>(),
                "",
                "",
                "",
                null,
                false,
                false);

        public Argument_Holder(string? path_to_schema, string? output_path, bool stdIn, TargetLanguage? language, ListImplementation? list_type, string? @namespace, string? encoding_classname, List<CustomType> customTypes, string customType_Name, string customType_Type, string customType_Encoder, int? identation_whitespaces, bool snakeCase, bool help)
        {
            Path_to_schema = path_to_schema;
            Output_path = output_path;
            StdIn = stdIn;
            Language = language;
            List_type = list_type;
            Namespace = @namespace;
            Encoding_classname = encoding_classname;
            CustomTypes = customTypes;
            CustomType_Name = customType_Name;
            CustomType_Type = customType_Type;
            CustomType_Encoder = customType_Encoder;
            Identation_whitespaces = identation_whitespaces;
            Snake_case = snakeCase;
            Help = help;
        }
    }

    internal static class CommandLine
    {
        public static Command Parse_command(string[] args)
        {
            if (args.Length == 0)
            {
                Print_error("Missing arguments");
                return new Print_help();
            }

            try
            {
                var arguments =
                    args.Aggregate(
                        (holder: Argument_Holder.Empty(), collector: Argument_Collector.None),
                        (state, arg) => Collect_argument(state.holder, state.collector, arg),
                        state => Arguments_validated(state.holder, state.collector));

                if (arguments.Help) return new Print_help();

                if (arguments.StdIn)
                {
                    return new Generate_from_StdIn_to_StdOut(
                        new Code_configuration(
                            arguments.Language,
                            arguments.Namespace,
                            arguments.List_type,
                            arguments.Encoding_classname,
                            arguments.CustomTypes,
                            arguments.Identation_whitespaces,
                            arguments.Snake_case));
                }

                if (arguments.Path_to_schema != null && arguments.Output_path != null)
                {
                    return new Generate_from_File(
                        arguments.Path_to_schema,
                        arguments.Output_path,
                        new Code_configuration(
                            arguments.Language,
                            arguments.Namespace,
                            arguments.List_type,
                            arguments.Encoding_classname,
                            arguments.CustomTypes,
                            arguments.Identation_whitespaces,
                            arguments.Snake_case));
                }

                return new Wrong_Arguments("Missing schema and output file arguments");
            }
            catch (ArgumentException ex)
            {
                return new Wrong_Arguments(ex.Message);
            }
            catch (Exception ex)
            {
                return new Internal_Error(ex);
            }
        }

        private static Argument_Holder Arguments_validated(Argument_Holder holder, Argument_Collector collector) =>
            collector switch
            {
                Argument_Collector.None => holder,
                Argument_Collector.Language => throw new ArgumentException("Missing option for --lang"),
                Argument_Collector.List_Type => throw new ArgumentException("Missing option for --list-type"),
                Argument_Collector.Namespace => throw new ArgumentException("Missing option for --namespace"),
                Argument_Collector.Classname => throw new ArgumentException("Missing option for --classname"),
                Argument_Collector.Identation_whitespaces => throw new ArgumentException("Missing option for --whitespaces"),
                Argument_Collector.Custom_Type_Name => throw new ArgumentException("Missing name, type, encoder and decoder for --custom"),
                Argument_Collector.Custom_Type_Type => throw new ArgumentException("Missing type, encoder and decoder for --custom"),
                Argument_Collector.Custom_Type_Encoder => throw new ArgumentException("Missing encoder and decoder for --custom"),
                Argument_Collector.Custom_Type_Decoder => throw new ArgumentException("Missing decoder for --custom"),
                _ => throw new ArgumentOutOfRangeException(nameof(collector), collector, null)
            };

        private static (Argument_Holder, Argument_Collector) Collect_argument(Argument_Holder holder, Argument_Collector collector, string arg)
        {
            switch(collector)
            {
                case Argument_Collector.Language:
                {
                    if (arg == "cs") holder.Language = TargetLanguage.CSharp;
                    else if (arg == "fs") holder.Language = TargetLanguage.FSharp;
                    else throw new ArgumentException($"Invalid option for --lang: '{arg}'");

                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.List_Type:
                {
                    if (arg == "list") holder.List_type = ListImplementation.List;
                    else if (arg == "array") holder.List_type = ListImplementation.Array;
                    else if (arg == "readonly") holder.List_type = ListImplementation.ReadonlyCollection;
                    else throw new ArgumentException($"Invalid option for --list-type: '{arg}'{Environment.NewLine}Possible values \"list\", \"array\" or \"readonly\"");

                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.Namespace:
                {
                    holder.Namespace = arg;
                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.Classname:
                {
                    holder.Encoding_classname = arg;
                    return (holder, Argument_Collector.None);
                }

                case Argument_Collector.Identation_whitespaces:
                {
                    if (int.TryParse(arg, out var count))
                    {
                        holder.Identation_whitespaces = count;
                        return (holder, Argument_Collector.None);
                    }
                    throw new ArgumentException($"Option for --whitespaces is not a number: '{arg}'");
                }

                case Argument_Collector.Custom_Type_Name:
                {
                    holder.CustomType_Name = arg;
                    return (holder, Argument_Collector.Custom_Type_Type);
                }
                case Argument_Collector.Custom_Type_Type:
                {
                    holder.CustomType_Type = arg;
                    return (holder, Argument_Collector.Custom_Type_Encoder);
                }
                case Argument_Collector.Custom_Type_Encoder:
                {
                    holder.CustomType_Encoder = arg;
                    return (holder, Argument_Collector.Custom_Type_Decoder);
                }
                case Argument_Collector.Custom_Type_Decoder:
                {
                    holder.CustomTypes.Add(new CustomType(holder.CustomType_Name, holder.CustomType_Type, holder.CustomType_Encoder, arg));
                    return (holder, Argument_Collector.None);
                }

                default:
                {
                    if (arg == "--stdin")
                    {
                        holder.StdIn = true;
                        return (holder, Argument_Collector.None);
                    }
                    if (arg == "--help")
                    {
                        holder.Help = true;
                        return (holder, Argument_Collector.None);
                    }
                    if (arg == "-sc" || arg == "--snake-case")
                    {
                        holder.Snake_case = true;
                        return (holder, Argument_Collector.None);
                    }
                    if (arg == "-l" || arg == "--lang") return (holder, Argument_Collector.Language);
                    if (arg == "-lt" || arg == "--list-type") return (holder, Argument_Collector.List_Type);
                    if (arg == "-ns" || arg == "--namespace") return (holder, Argument_Collector.Namespace);
                    if (arg == "-class" || arg == "--classname") return (holder, Argument_Collector.Classname);
                    if (arg == "-ws" || arg == "--whitespaces") return (holder, Argument_Collector.Identation_whitespaces);
                    if (arg == "-c" || arg == "--custom") return (holder, Argument_Collector.Custom_Type_Name);

                    if (holder.Path_to_schema == null)
                    {
                        holder.Path_to_schema = arg;
                        return (holder, Argument_Collector.None);
                    }
                    else if (holder.Output_path == null)
                    {
                        holder.Output_path = arg;
                        return (holder, Argument_Collector.None);
                    }

                    throw new ArgumentException($"Unknown argument '{arg}'");
                }
            }
        }

        public static void Print_help()
        {
            Print_in_color(ConsoleColor.DarkGray, "Usage");
            Print_in_color(ConsoleColor.White, "Read schema from file and writes to out file".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "bare <schema-file> <out-file>".With_indentation(2, null));
            Print_in_color(ConsoleColor.White, "Read schema from stdin and writes to stdout".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "bare --stdin".With_indentation(2, null));

            Console.WriteLine();
            Print_in_color(ConsoleColor.DarkGray, "Examples");
            Print_in_color(ConsoleColor.Blue, "bare schema.bare Messages.cs".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "echo \"type Token data<16>\" | bare --stdin | less".With_indentation(1, null));
            Print_in_color(ConsoleColor.Blue, "echo \"type UUID data<16>\" | bare --stdin -c UUID System.Guid MyEncoding.encode_Guid MyEncoding.decode_Guid | less".With_indentation(1, null));

            Console.WriteLine();
            Print_in_color(ConsoleColor.DarkGray, "Options");
            Print_in_color(ConsoleColor.White, "--help".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Prints this help text.".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "--stdin".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Reads schema from stdin and writes code to stdout.".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-l --lang <fs|cs>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "The target language to produce code for.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: cs".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-lt --list-type <list|array|readonly>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Defines which underlying type the code generator should use for lists.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: array".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-ns --namespace <name>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Types and encoding class will be in the given namespace.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: \"Bare.Msg\"".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-class --classname <name>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Name of the encoding class".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default: \"Encoding\"".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-c --custom <name> <type> <encoder> <decoder>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Inject a custom type in the generated code. Multiple custom types possible.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "name - type name declared in the schema file, which will be replaced.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "type - full name of the custom type".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "encoder - full name of the encoder function".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "decoder - full name of the decoder function".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-ws --whitespaces <count>".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Count of whitespaces used for indentation. When set the C# code will be indentated with whitespaces instead of tabs.".With_indentation(2, null));
            Print_in_color(ConsoleColor.Gray, "default for F#: 2".With_indentation(2, null));

            Print_in_color(ConsoleColor.White, "-sc --snake-case".With_indentation(1, null));
            Print_in_color(ConsoleColor.Gray, "Converts type and field names to Snake_case".With_indentation(2, null));
        }

        public static void Print_error(string text) => Print_in_color(ConsoleColor.Red, text);

        private static void Print_in_color(ConsoleColor color, string text)
        {
            var tmp = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = tmp;
        }

        internal static void Write_error(Exception ex)
        {
            Console.Error.WriteLine(ex.Message);
            Console.Error.WriteLine(ex.StackTrace);
        }
    }
}