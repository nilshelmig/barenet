﻿#nullable enable
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BareNET.Schema;

namespace Bare.CodeGen
{
    public class Program
    {
        public static int Main(string[] args)
        {
            switch (CommandLine.Parse_command(args))
            {
                case Generate_from_File command:
                {
                    if (!File.Exists(command.Path_to_schema))
                    {
                        CommandLine.Print_error($"Given path of schema file leads to nowhere. Please check the path {command.Path_to_schema}");
                        return 1;
                    }

                    try
                    {
                        var schema = Read_schema(command.Path_to_schema);
                        var code = Generate_code(schema, command.Configuration);
                        Save_code_to_file(command.Output_path, code);
                    }
                    catch (Exception ex)
                    {
                        CommandLine.Print_error(ex.Message);
                        return 1;
                    }

                    break;
                }

                case Generate_from_StdIn_to_StdOut command:
                {
                    try
                    {
                        var schema = Read_schema_from_stdin();
                        var code = Generate_code(schema, command.Configuration);
                        Save_code_to_stdout(code);
                    }
                    catch(Exception ex)
                    {
                        CommandLine.Write_error(ex);
                        return 1;
                    }
                    break;
                }

                case Print_help:
                {
                    CommandLine.Print_help();
                    break;
                }

                case Wrong_Arguments error:
                {
                    CommandLine.Print_error(error.Message);
                    return 1;
                }

                case Internal_Error error:
                {
                    CommandLine.Print_error("Internal error while parsing arguments");
                    CommandLine.Print_error(error.Exception.Message);
                    return 1;
                }
            }

            return 0;
        }

        private static List<UserType> Read_schema_from_stdin()
        {
            using var stream = Open_stdin();
            using var scanner = new Scanner(new StreamReader(stream));
            return Parse_schema(scanner);
        }

        private static Stream Open_stdin()
        {
            try
            {
                Console.InputEncoding = System.Text.Encoding.UTF8;
                return Console.OpenStandardInput();
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to parse schema from stdin.{Environment.NewLine}{ex.Message}", ex);
            }
        }

        private static List<UserType> Read_schema(string path)
        {
            using var file = Open_file(path);
            using var scanner = new Scanner(new StreamReader(file));
            return Parse_schema(scanner);
        }

        private static FileStream Open_file(string path)
        {
            try
            {
                return File.OpenRead(path);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to read schema file '{path}'.{Environment.NewLine}{ex.Message}", ex);
            }
        }

        private static List<UserType> Parse_schema(Scanner scanner)
        {
            try
            {
                return Parser.Parse(scanner).ToList();
            }
            catch (FormatException ex)
            {
                throw new Exception($"Syntax error in schema: {ex.Message}", ex);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed parse schema.{Environment.NewLine}{ex.Message}", ex);
            }
        }

        private static string Generate_code(List<UserType> schema, Code_configuration configuration)
        {
            try
            {
                return configuration.Target_language is TargetLanguage.FSharp
                    ? FSharp_code_generator.Generate(schema, DateTime.Now, Generator_configuration.From_code_configuration(configuration))
                    : CSharp_code_generator.Generate(schema, DateTime.Now, Generator_configuration.From_code_configuration(configuration));
            }
            catch (FormatException ex)
            {
                throw new Exception($"Schema error: {ex.Message}", ex);
            }
            catch (Exception ex)
            {
                throw new Exception($"Internal error during code generation.{Environment.NewLine}{ex.Message}", ex);
            }
        }

        private static void Save_code_to_file(string file, string code)
        {
            File.WriteAllText(file, code);
        }

        private static void Save_code_to_stdout(string code)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Out.Write(code);
        }
    }
}
