# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.7.0] - 2023-01-12
### Changed
- Update schema parser to Bare schema spec v7

## [0.6.0] - 2021-12-11
### Changed
- Support .NET 6 SDK

## [0.5.0] - 2021-05-05
### Added
- [CodeGen] Option to convert type and field names to snake_case.
  - Can be activated with CLI argument `-sc` or `--snake-case`.

### Changed
- [CodeGen] **BREAKING!** Naming of union members changed to prevent conflicting names.
  - Every anonymouse type in union member will follow the convetion `{TYPE}Case{IDENTIFIER}` or in snake case `{TYPE}_case_{IDENTIFIER}`
  - F# union cases will no longer have the union name in it. This increases the readability.
  - F# field names will begin with an uppercase character.

### Removed
- [CodeGen][F#] Don't generate type alias for `unit` types used in unions.

## [0.4.2] - 2021-03-27
### Fixed
- [CodeGen][F#] Fixed F# type of `f32` being `Double`.

## [0.4.1] - 2021-03-20
### Fixed
- [CodeGen][F#] Decoding of struct works, if multiple type share the same structure.

## [0.4.0] - 2021-03-13
### Added
- Option to inject custom types for the code generator.
  - This allows usage of already defined Types in combination with generated types.
  - Use with option `--custom <name> <type> <encoder> <decoder>`
  - Example `bare --custom UUID System.Guid MyEncoding.Encode_Guid MyEncoding_Decode_Guid`
- [CLI] Return exit code 1 on error.

### Changed
- [CodeGen][C#] Omit encoding class if there would be no content.
- [CLI] Better error messages when cli arguments are missing.

### Fixed
- [CodeGen][F#] Argument names of named and anonymous records are now lowercase to follow F# naming convention. See #7
- [CLI] Wrong default value of `--list-type`. See #6

## [0.3.0] - 2021-03-04
### Changed
- [BREAKING] For F# code, encoding functions will be named as `to[Typename]` and value decoding function as `of[Typename]`.
	- This follows the naming conventions like in `List.ofSeq` and `List.toSeq`.
	- Generated decoder will still be named as `decode_[Typename]`.
	- Thanks to Alex Corrado!
- [CodeGen][F#] Don't expose union encode and decode definitions.
- CLI has readable, colored and meaningful error messages. No more error message hidden in a call stack!

### Fixed
- [F#] Type order will be determined correctly. The algorithm will consider nested usages.

## [0.2.0] - 2021-03-03
### Added
- [CodeGen] Generation of F# code. `bare schema.bare out.fs --lang fs`
- Option to read schema from stdin and write code to stdout. `bare --stdin`
- Option to change namespace of generated code. `bare --namespace My_own_Namespace`
- Option to change name of encoding class. `bare --classname Stuff_n_Things`
- Option to change the type for list values. `bare --list-type <list|array|readonly>`
  - F# will map `array` to `T array`
  - F# will map `list` and `readonly` to `T list`
  - C# will map `array` to `T[]`
  - C# will map `list` to `List<T>`
  - C# will map `readonly` to `ReadOnlyCollection<T>`
- Option to change the whitespace indentation. `bare --whitespace 4`
  - For C# this switches to whitespace indentation instead of tabs.
- CLI can print a beautiful help message. `bare --help`

### Fixed
- [CodeGen] Generator will not break struct fields CamelCase. See #3

## [0.1.2] - 2021-02-14
### Fixed
- [AST] Parser can now handle end of stream and won't fail if a schema ends with a trailing newline. See #1

## [0.1.1] - 2021-02-11
### Fixed
- [CodeGen] Structs with single field will now encode the field correctly.

## [0.1.0] - 2021-02-10
### Added
- Initial release