﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BareNET.Schema
{
    public static class Parser
    {
        public static IEnumerable<UserType> Parse(Scanner scanner)
        {
            while (!scanner.EndOfStream)
            {
                var next = Lexer.Next(scanner);
                if (!next.HasValue) break;

                yield return ParseUserTypes(next.Value.token, scanner);
            }
        }

        private static UserType ParseUserTypes(Token token, Scanner scanner)
        {
            return token switch
            {
                Token.Type => ParseUserType(scanner),
                _ => throw new FormatException($"Expected token 'type' but got {token}")
            };
        }

        private static bool Is_unallowed_typename_letter(char c) => !char.IsDigit(c) && !(c >= 'A' && c <= 'Z') && !(c >= 'a' && c <= 'z');

        private static UserType ParseUserType(Scanner scanner)
        {
            var word = Lexer.Next(scanner);
            if (!word.HasValue) throw new FormatException("Expected WORD but got nothing");

            if (word.Value.token != Token.Word) throw new FormatException($"Expected WORD but got {word.Value}");
            var name = word.Value.value;
            if (char.IsLower(name, 0)) throw new FormatException("Type must start with uppercase letter");
            if (name.Any(Is_unallowed_typename_letter)) throw new FormatException("Only letter and digits are allowed for type names");

            return new UserType(name, ParseAnyType(scanner));
        }

        private static AnyType ParseAnyType(Scanner scanner)
        {
            var next = Lexer.Next(scanner);
            if (!next.HasValue) throw new FormatException("Expected type specification but got nothing");
            var (token, value) = next.Value;

            return token switch
            {
                Token.UInt => new PrimitiveType(TypeKind.UInt),
                Token.U8 => new PrimitiveType(TypeKind.U8),
                Token.U16 => new PrimitiveType(TypeKind.U16),
                Token.U32 => new PrimitiveType(TypeKind.U32),
                Token.U64 => new PrimitiveType(TypeKind.U64),
                Token.Int => new PrimitiveType(TypeKind.Int),
                Token.I8 => new PrimitiveType(TypeKind.I8),
                Token.I16 => new PrimitiveType(TypeKind.I16),
                Token.I32 => new PrimitiveType(TypeKind.I32),
                Token.I64 => new PrimitiveType(TypeKind.I64),
                Token.F32 => new PrimitiveType(TypeKind.F32),
                Token.F64 => new PrimitiveType(TypeKind.F64),
                Token.Bool => new PrimitiveType(TypeKind.Bool),
                Token.String => new PrimitiveType(TypeKind.String),
                Token.Void => new PrimitiveType(TypeKind.Void),
                Token.Data => ParseDataType(scanner),
                Token.Enum => ParseEnumType(scanner),
                Token.Optional => ParseOptionalType(scanner),
                Token.Map => ParseMapType(scanner),
                Token.List => ParseListType(scanner),
                Token.Union => ParseUnionType(scanner),
                Token.Struct => ParseStructType(scanner),
                Token.Word => new UserTypeName(value),
                _ => throw new FormatException($"Unexpected token {token}")
            };
        }

        private static DataType ParseDataType(Scanner scanner)
        {
            return new DataType(ParseLength(scanner, "data"));
        }

        private static EnumType ParseEnumType(Scanner scanner)
        {
            var leftBrace = Lexer.Next(scanner);
            if (!leftBrace.HasValue || leftBrace.Value.token != Token.LeftBrace) throw new FormatException("Missing '{' after 'enum'");
            return new EnumType(ParseEnumValues(scanner).ToArray());
        }

        private static bool Is_unallowed_enum_letter(char c) => c < 'A' || c > 'Z';

        private static List<EnumValue> ParseEnumValues(Scanner scanner)
        {
            var result = new List<EnumValue>();
            while (true)
            {
                var word = Lexer.Next(scanner);
                if (!word.HasValue || word.Value.token != Token.Word) throw new FormatException("Missing name for enum field");
                var name = word.Value.value;
                if (name.Any(c => !char.IsDigit(c) && c != '_' && Is_unallowed_enum_letter(c)))
                {
                    throw new FormatException("enum value must be all uppercase or contains numbers or '_'");
                }

                var next = Lexer.Next(scanner);
                if (!next.HasValue) throw new FormatException("Missing '}' for enum");
                if (next.Value.token == Token.Equal)
                {
                    var digit = Lexer.Next(scanner);
                    if (!digit.HasValue || digit.Value.token != Token.Digit) throw new FormatException("Missing number after '=' for enum value");
                    result.Add(new EnumValue(name, uint.Parse(digit.Value.value)));
                    next = Lexer.Next(scanner);
                    if (!next.HasValue) throw new FormatException("Missing '}' for enum");
                }
                else result.Add(new EnumValue(name, null));

                if (next.Value.token == Token.RightBrace) break;

                scanner.Unread();
            }
            return result;
        }

        private static OptionalType ParseOptionalType(Scanner scanner)
        {
            var leftAngle = Lexer.Next(scanner);
            if (!leftAngle.HasValue || leftAngle.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'optional'");
            var type = ParseAnyType(scanner);
            var rightAngle = Lexer.Next(scanner);
            if (!rightAngle.HasValue || rightAngle.Value.token != Token.RightAngle) throw new FormatException("Missing '>' after 'optional<'");
            return new OptionalType(type);
        }

        private static ulong? ParseLength(Scanner scanner, string dataType) {
            var leftBracket = Lexer.Next(scanner);
            if (!leftBracket.HasValue || leftBracket.Value.token != Token.LeftBracket)
            {
                if (leftBracket.HasValue) scanner.Unread();
                return null;
            }

            var digit = Lexer.Next(scanner);
            if (!digit.HasValue || digit.Value.token != Token.Digit) throw new FormatException($"Expected a number after '${dataType}['");
            var length = ulong.Parse(digit.Value.value);
            if (length == 0) throw new InvalidDataException($"Length of '{dataType}' must be at least 1");

            var rightBracket = Lexer.Next(scanner);
            if (!rightBracket.HasValue || rightBracket.Value.token != Token.RightBracket) throw new FormatException($"Missing ']' after '${dataType}[${length}'");

            return length;
        }

        private static bool Is_unallowed_key_type(AnyType type)
        {
            return type is DataType
                || type is PrimitiveType t && (t.Kind == TypeKind.F32 || t.Kind == TypeKind.F64 || t.Kind == TypeKind.Void);
        }

        private static MapType ParseMapType(Scanner scanner)
        {
            var leftAngleKey = Lexer.Next(scanner);
            if (!leftAngleKey.HasValue || leftAngleKey.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'map'");

            var keyType = ParseAnyType(scanner);
            if (Is_unallowed_key_type(keyType)) throw new InvalidDataException("Key type of map must be a primitive type which is not f32, f64, data, data[] or void");

            var rightAngleKey = Lexer.Next(scanner);
            if (!rightAngleKey.HasValue || rightAngleKey.Value.token != Token.RightAngle) throw new FormatException("Expected '>' after 'map<'");

            var leftAngleVal = Lexer.Next(scanner);
            if (!leftAngleVal.HasValue || leftAngleVal.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'map<key_type>'");

            var valueType = ParseAnyType(scanner);

            var rightAngleVal = Lexer.Next(scanner);
            if (!rightAngleVal.HasValue || rightAngleVal.Value.token != Token.RightAngle) throw new FormatException("Expected '>' after 'map<key_type><'");

            return new MapType(keyType, valueType);
        }

        private static ListType ParseListType(Scanner scanner)
        {
            var leftAngle = Lexer.Next(scanner);
            if (!leftAngle.HasValue || leftAngle.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'list'");

            var listType = ParseAnyType(scanner);

            var rightAngle = Lexer.Next(scanner);
            if (!rightAngle.HasValue || rightAngle.Value.token != Token.RightAngle) throw new FormatException("Expected '>' after 'list<'");

            return new ListType(listType, ParseLength(scanner, $"list<>"));
        }

        private static UnionType ParseUnionType(Scanner scanner)
        {
            var next = Lexer.Next(scanner);
            if (!next.HasValue || next.Value.token != Token.LeftBrace) throw new FormatException("Missing { after 'union'");

            return new(ParseUnionMembers(scanner).ToArray());
        }

        private static List<UnionMember> ParseUnionMembers(Scanner scanner)
        {
            var result = new List<UnionMember>();
            while (true)
            {
                var next = Lexer.Next(scanner);
                if (next.HasValue)
                {
                    if (next.Value.token == Token.RightBrace) break;
                    if (next.Value.token != Token.Pipe) scanner.Unread();
                }

                var type = ParseAnyType(scanner);

                next = Lexer.Next(scanner);
                if (!next.HasValue) throw new FormatException("Missing '}' for union");

                if (next.Value.token == Token.Equal)
                {
                    var digit = Lexer.Next(scanner);
                    if (!digit.HasValue || digit.Value.token != Token.Digit) throw new FormatException("Expected number after '=' for union member");
                    result.Add(new UnionMember(type, uint.Parse(digit.Value.value)));
                    var nextToken = Lexer.Next(scanner);
                    if (!nextToken.HasValue) throw new FormatException("Missing '}' for union");
                    if (nextToken.Value.token == Token.RightBrace) break;
                    if (nextToken.Value.token == Token.Pipe) continue;
                }
                else if (next.Value.token == Token.Pipe)
                {
                    result.Add(new UnionMember(type, null));
                    continue;
                }
                else if (next.Value.token == Token.RightBrace)
                {
                    result.Add(new UnionMember(type, null));
                    break;
                }

                throw new FormatException($"Unexpected token {next.Value.token} for union");
            }

            if (result.Count == 0) throw new FormatException("Type 'union' must have at least one member");
            return result;
        }

        private static StructType ParseStructType(Scanner scanner)
        {
            var next = Lexer.Next(scanner);
            if (!next.HasValue || next.Value.token != Token.LeftBrace) throw new FormatException("Missing { after 'struct'");

            var fields = ParseStructFields(scanner).ToArray();
            if (fields.Length == 0) throw new FormatException("Structs must have at least 1 field");
            return new StructType(fields);
        }

        private static bool Is_unallowed_struct_field_letter(char c) => !(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z');

        private static List<StructField> ParseStructFields(Scanner scanner)
        {
            var result = new List<StructField>();
            while (true)
            {
                var word = Lexer.Next(scanner);
                if (!word.HasValue || word.Value.token != Token.Word) throw new FormatException("Missing name for struct field");
                if (word.Value.value.Any(Is_unallowed_struct_field_letter)) throw new FormatException("Field name can only contains letters");

                var colon = Lexer.Next(scanner);
                if (!colon.HasValue || colon.Value.token != Token.Colon) throw new FormatException("Missing ':' for struct field");

                var type = ParseAnyType(scanner);
                if (type is PrimitiveType t && t.Kind == TypeKind.Void) throw new InvalidDataException("");
                result.Add(new StructField(word.Value.value, type));

                var rightBrace = Lexer.Next(scanner);
                if (!rightBrace.HasValue) throw new FormatException("Missing '}' for struct");
                if (rightBrace.Value.token == Token.RightBrace) break;
                scanner.Unread();
            }
            return result;
        }
    }
}