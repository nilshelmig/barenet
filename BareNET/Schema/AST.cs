﻿namespace BareNET.Schema
{
    public interface AnyType { }

    public enum TypeKind
    {
        Int,
        I8,
        I16,
        I32,
        I64,
        UInt,
        U8,
        U16,
        U32,
        U64,
        F32,
        F64,
        Bool,
        String,
        Void
    }

    public readonly struct UserType
    {
        public readonly string Name;
        public readonly AnyType Type;

        public UserType(string name, AnyType type)
        {
            Name = name;
            Type = type;
        }
    }

    public readonly struct PrimitiveType : AnyType
    {
        public readonly TypeKind Kind;

        public PrimitiveType(TypeKind kind)
        {
            Kind = kind;
        }
    }

    public readonly struct DataType : AnyType
    {
        public readonly ulong? Length;

        public DataType(ulong? length)
        {
            Length = length;
        }
    }

    public readonly struct UserTypeName : AnyType
    {
        public readonly string Name;

        public UserTypeName(string name)
        {
            Name = name;
        }
    }

    public readonly struct EnumType : AnyType
    {
        public readonly EnumValue[] Values;

        public EnumType(EnumValue[] values)
        {
            Values = values;
        }

        public bool Equals(EnumType other)
        {
            if (Values.Length != other.Values.Length) return false;
            for (var i = 0; i < Values.Length; i++)
            {
                if (!Values[i].Equals(other.Values[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is EnumType other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Values.GetHashCode();
        }
    }

    public readonly struct EnumValue
    {
        public readonly string Name;
        public readonly uint? Value;

        public EnumValue(string name, uint? value)
        {
            Name = name;
            Value = value;
        }
    }

    public readonly struct OptionalType : AnyType
    {
        public readonly AnyType Type;

        public OptionalType(AnyType type)
        {
            Type = type;
        }
    }

    public readonly struct ListType : AnyType
    {
        public readonly AnyType Type;
        public readonly ulong? Length;

        public ListType(AnyType type, ulong? length)
        {
            Type = type;
            Length = length;
        }
    }

    public readonly struct MapType : AnyType
    {
        public readonly AnyType KeyType;
        public readonly AnyType ValueType;

        public MapType(AnyType keyType, AnyType valueType)
        {
            KeyType = keyType;
            ValueType = valueType;
        }
    }

    public readonly struct UnionType : AnyType
    {
        public readonly UnionMember[] Members;

        public UnionType(UnionMember[] members)
        {
            Members = members;
        }

        public bool Equals(UnionType other)
        {
            if (Members.Length != other.Members.Length) return false;
            for (var i = 0; i < Members.Length; i++)
            {
                if (!Members[i].Equals(other.Members[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is UnionType other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Members.GetHashCode();
        }
    }

    public readonly struct UnionMember
    {
        public readonly AnyType Type;
        public readonly uint? Identifier;

        public UnionMember(AnyType type, uint? identifier)
        {
            Type = type;
            Identifier = identifier;
        }
    }

    public readonly struct StructType : AnyType
    {
        public readonly StructField[] Fields;

        public StructType(StructField[] fields)
        {
            Fields = fields;
        }

        public bool Equals(StructType other)
        {
            if (Fields.Length != other.Fields.Length) return false;
            for (var i = 0; i < Fields.Length; i++)
            {
                if (!Fields[i].Equals(other.Fields[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is StructType other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Fields.GetHashCode();
        }
    }

    public readonly struct StructField
    {
        public readonly string Name;
        public readonly AnyType Type;

        public StructField(string name, AnyType type)
        {
            Name = name;
            Type = type;
        }
    }
}